create table if not exists aquariums (
    id uuid unique not null,
    name varchar(200) not null,
    description text null,
	volume integer null,
	start timestamp null,
	constraint aquarium_key foreign key (id) references items (id)
);

/*create unique index unique_aquarium_1
    on aquariums
    using btree (name, description, volume)
    where aquarium_name is not null and aquarium_description is not null and aquarium_volume is null;

create unique index unique_aquarium_2
    on aquariums
    using btree (name, description, volume)
    where aquarium_name is not null and aquarium_volume is not null and aquarium_description is null;

create unique index unique_aquarium_3
    on aquariums
    using btree (name, description, volume)
    where aquarium_name is not null and aquarium_volume is null and aquarium_description is null;

create unique index unique_aquarium_4
    on aquariums
    using btree (name, description, volume)
    where aquarium_name is not null and aquarium_description is not null and aquarium_volume is not null;
*/