create type labelcolor as enum ('GRAY', 'BROWN', 'BLUE', 'GREEN', 'RED', 'YELLOW');
create type itemtype as enum ('AQUARIUM', 'DEVICE', 'PET', 'PLANT', 'NOTE', 'TASK', 'EXPERIMENT');

create table if not exists labels (
    id uuid primary key default uuid_generate_v4(),
    owner bigint not null,
    type itemtype not null,
    name varchar(200) not null,
    color labelcolor not null default 'GRAY',
    delete boolean not null default false,
    add timestamp not null,
    edit timestamp not null,
    constraint label_key foreign key (owner) references users (id)
);

create table if not exists aquarium_labels (
    id uuid not null,
    item_id uuid not null,
	constraint label_key foreign key (id) references labels (id),
	constraint labelItem_key foreign key (item_id) references items (id)
);