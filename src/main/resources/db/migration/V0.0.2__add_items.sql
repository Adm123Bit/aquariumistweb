create type sharetype as enum ('COMMON', 'APPUSER', 'PRIVATE');

create table if not exists items (
    id uuid primary key default uuid_generate_v4(),
	owner bigint not null,
	share sharetype not null,
	archive boolean not null default false,
	delete boolean not null default false,
	add timestamp not null,
	edit timestamp not null,
	constraint user_key foreign key (owner) references users (id)
);