create extension if not exists "uuid-ossp";
create type userrole as enum ('ADMIN', 'USER');

create table if not exists users (
    id bigserial primary key,
    login varchar(30) not null unique,
    password varchar(100) not null
);

create table if not exists user_data (
    id bigserial not null unique,
    role userrole not null default 'USER',
    constraint user_id_key foreign key (id) references users (id)
);

create table if not exists user_session (
    session_id uuid primary key,
    user_id bigint not null
);