$(document).ready(() => {
	setAuthorizationEventHandlers();
});

function setAuthorizationEventHandlers() {

    // Отправка формы авторизации
    $('#buttonLogin').off('click');
    $('#buttonLogin').on('click', () => {
        sendFormLogin();
    });

    // Отправка формы регистрации
    $('#buttonRegister').off();
    $('#buttonRegister').on('click', () => {
        sendFormRegister();
    });

    // Клик по кнопке Да на всплывалке с вопросом о разлогинке
    $('#buttonLogout').off('click');
    $('#buttonLogout').on('click', () => {
        sendLogoutForm();
    });
    UIkit.util.on('#modalLogin', 'beforeshow', () => {
        clearForm('#formLogin');
    });

/*
    $('#loginForAuth').off();
    $('#loginForAuth').on('input', () => {
        clearDangerInput('loginForAuth');
    });

    $('#passwordForAuth').off();
    $('#passwordForAuth').on('input', () => {
        clearDangerInput('passwordForAuth');
    });

    $('#loginForRegister').off();
    $('#loginForRegister').on('input', () => {
        clearDangerInput('loginForRegister');
    });

    $('#passwordForRegister').off();
    $('#passwordForRegister').on('input', () => {
        clearDangerInput('passwordConfirmForRegister');
        clearDangerInput('passwordForRegister');
    });

    $('#passwordConfirmForRegister').off();
    $('#passwordConfirmForRegister').on('input', () => {
        clearDangerInput('passwordConfirmForRegister');
        clearDangerInput('passwordForRegister');
    });*/

}

// Отправка формы авторизации
function sendFormLogin() {
    userLogin = $('#loginForAuth').val();
    userPass = $('#passwordForAuth').val();
    checkRememberMe = $('#checkRememberMe').prop('checked');
    var validForm = true;
/*    if (userLogin.trim().length < 3) {
        //setDangerInput('loginForAuth');
        //validForm = false;
    }*/
/*    if (userPass.trim().length < 4) {
        //setDangerInput('passwordForAuth');
        //validForm = false;
        showError(messages.incorrectsomedata);
    } */
/*        clearDangerInput('loginForAuth');
        clearDangerInput('passwordForAuth');*/
    const loginData = {
        login               : userLogin,
        password            : userPass,
        pageTemplate        : pageTemplate,
        contentOwnerLogin   : contentOwnerLogin,
        itemId              : itemId,
        check               : checkRememberMe
    };
    //clearForm('#formLogin');
    $.ajax({
        url			: '../../profile/login',
        data		: JSON.stringify(loginData)
    }).done(response => {
        refreshDOM(response);
        closeModal();
    }).fail((response, status) => {
        responseFail(response);
        //showError(messages.get('unknownerror'));
    });
}

// Отправка формы регистрации
function sendFormRegister() {
    userPass1 = $('#passwordForRegister').val();
    userPass2 = $('#passwordConfirmForRegister').val();
    userLogin = $('#loginForRegister').val();
    validForm = true;
    if (userLogin.trim().length < 3) {
        showError(messages.get('incorrectlogin'));
        validForm = false;
    }
    if (userPass1 != userPass2) {
        showError(messages.get('passworddifference'));
        validForm = false;
    } else if (!passwordRegExp.test(userPass1)) {
        showError(messages.get('incorrectpassword'));
        validForm = false;
    }
    if (validForm) {
        const registerData = {
            login           : userLogin,
            password        : userPass1
            //passwordConfirm : userPass2,
            //check           : false
        };
        $.ajax({
            url			: '../../profile/register',
            data		: JSON.stringify(registerData)
            //dataType    : 'application/json'
        }).done(response => {
            //clearForm('#formRegister');
            showSuccess(messages.get('accountcreate'));
            //clearForm('#formRegister');
            refreshDOM(response);
        }).fail((response, error) => {
            responseFail(response);
        });
	}
}

/*// Успешное создание аккаунта
function successAccountCreate() {
    clearForm('#formRegister');
    showSuccess(messages.get('accountcreate'));
}*/

// Успешная авторизация
function successLogin() {
    //UIkit.toggle('#closeformLogin').toggle();
    UIkit.modal('#modalLogin').hide();
}

// Выход из аккаунта
function sendLogoutForm() {
    const pageData = {
        pageTemplate        : pageTemplate,
        contentOwnerLogin   : contentOwnerLogin,
        itemId              : itemId
    };
	$.ajax({
		url			: '../../profile/logout',
		data		: JSON.stringify(pageData)
	}).done(response => {
	    refreshDOM(response);
	    closeModal();
    }).fail((response, status) => {
        showError(messages.get('unknownerror'));
    });
}