// Сбор данных об аквариуме с его карточки для передачи в форму редактирования
function collectItemDataFromCard(editIcon) {
    dtoSpan = editIcon.closest('div.item').find('span.dtoData')
    id = dtoSpan.attr('id');
    name = dtoSpan.attr('name');
    volume = dtoSpan.attr('volume')
    description = dtoSpan.attr('description');
    startDate = dtoSpan.attr('startDate');
    share = dtoSpan.attr('share');
    archived = dtoSpan.attr('archived') == 'true';
    addDateTime = dtoSpan.attr('addDateTime');
    //labels = collectLabels(labelSpan);
    $('#aquariumIdEdit').val(id);
    $('#itemNameEdit').val(name);
    $('#itemDescriptionEdit').val(description);
    $('#aquariumVolumeEdit').val(volume);
    $('#aquariumSatrtDateEdit').val(startDate);
    $('#aquariumVolumeContainerEdit').hide();
    if (volume === undefined) {
        $('#aquariumVolumeContainerEdit').show();
    }
    $('#aquariumStartDateContainerEdit').hide();
    if (startDate === undefined) {
        $('#aquariumStartDateContainerEdit').show();
    }
    $('#itemShareEdit option[value = "' + share + '"]').prop('selected', true);
    $('#itemArchivedEdit').prop('checked', archived);
}

// Отправка формы правки аквариума
function sendFormEditItem() {
    aquariumId = $('#aquariumIdEdit').val();
    aquariumName = $('#itemNameEdit').val();
    aquariumVolume = $('#aquariumVolumeEdit').val();
    aquariumStartDate = $('#aquariumStartDateEdit').val();
    aquariumDescription = $('#itemDescriptionEdit').val();
    aquariumShare = $('#itemShareEdit option:selected').val();
    aquariumArchived = $('#itemArchivedEdit').prop('checked');
    var validForm = true;
    if (isEmpty(aquariumName) || aquariumName.length < 3) {
        validForm = false;
        //setDangerInput('itemNameEdit');
    }
    if (compareDates(aquariumStartDate) > 0) {
        validForm = false;
        //setDangerInput('aquariumStartAdd');
    }
    if (!validForm) {
        showError(messages.get('incorrectsomedata'));
    } else {
        const aquariumData = {
            id          : aquariumId,
            share       : aquariumShare,
            name        : aquariumName.trim(),
            description : aquariumDescription.trim(),
            archived    : aquariumArchived,
            volume      : aquariumVolume,
            startDate   : aquariumStartDate,
            labelIds    : getItemLabelIds(),
            pageTemplate: pageTemplate
        };
        $.ajax({
            url			: '../../aquarium/edit',
            data		: JSON.stringify(aquariumData)
            }).done(response => {
                showSuccess(messages.get('successedit'));
                refreshDOM(response);
                closeModal();
            }).fail((response, error) => {
                responseFail(response);
            });
    }
}

// Отправка формы добавления акварума
function sendFormAddItem() {
    aquariumName = $('#aquariumNameAdd').val();
    aquariumDescription = $('#aquariumDescriptionAdd').val();
    aquariumVolume = $('#aquariumVolumeAdd').val();
    aquariumStartDate = $('#aquariumStartDateAdd').val();
    aquariumShare = $('#aquariumShareAdd option:selected').val();
    aquariumArchived = $('#aquariumArchivedAdd').prop('checked');
    var validForm = true;
    if (isEmpty(aquariumName) || aquariumName.length < 3) {
        validForm = false;
        //setDangerInput('aquariumNameAdd');
    }
    if (compareDates(aquariumStartDate) > 0) {
        validForm = false;
        //setDangerInput('aquariumStartAdd');
    }
    if (!validForm) {
        showError(messages.get('incorrectsomedata'));
    } else {
        const aquariumData = {
            share       : aquariumShare,
            archived    : aquariumArchived,
            name        : aquariumName.trim(),
            description : aquariumDescription.trim(),
            volume      : aquariumVolume,
            startDate   : aquariumStartDate
        };
        $.ajax({
            url			: '../../aquarium/add',
            data		: JSON.stringify(aquariumData)
            }).done(response => {
                showSuccess(messages.get('successadd'));
                refreshDOM(response);
                closeModal();
            }).fail((response, error) => {
                responseFail(response);
            });
    }
}
