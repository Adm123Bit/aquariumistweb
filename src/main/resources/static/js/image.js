$(document).ready(() => {
	setImageEventHandlers();
	//deletedImages = new Object();
});

function setImageEventHandlers() {
// Перед показом модалки управления картинками
    UIkit.util.on('#modalItemImages', 'beforeshow', () => {
        getItemImages().forEach((fileData, i, array) => {
            addImageToModal(fileData.fileName, '../../picture/' + fileData.ownerLogin + '/' + fileData.itemId + '/' + fileData.fileName);
        });
    });
// Клик по кнопке добавки картинки к итему в модалке редактуры картинок (вызов диалога)
    $('#iemImageAdd').off('click');
    $('#iemImageAdd').on('click', () => {
        $('#inputItemImage').trigger('click');
    });
// Слушалка изменения содержимого инпута новой картинки
    $('#inputItemImage').off('click');
    $('#inputItemImage').on('change', e => {
        imageUpload(e);
    });
/*// Клик по превьюшке в модалке управления картинками итема
    $('div.itemPreview').off('click');
    $('div.itemPreview').on('click', e => {
        previewCover = $(e.currentTarget).find('div.itemPreviewCover');
        previewCover.toggleClass('uk-hidden');
    });*/
/*// Клик по иконке удаления картинки (на выбранной превьюшке в модалке)
    $('a.itemPreviewDelete').off('click');
    $('a.itemPreviewDelete').on('click', e => {
        itemPreview = $(e.currentTarget).closest('div.itemPreview');
        itemPreview.addClass('uk-hidden');
        //$('div.itemPreview').show();
    });*/
// Клик по кнопке СОХРАНИТЬ модалки управления картинками итема
    $('#imageSave').off('click');
    $('#imageSave').on('click', () => {
        sendQueryDeleteImage();
    });
}

// Клик по превьюшке в модалке управления картинками итема
function setPreviewClick(preview) {
    //$('div.itemPreview').off('click');
    $(preview).on('click', e => {
        previewCover = $(preview).find('div.itemPreviewCover');
        previewCover.toggleClass('uk-hidden');
    });
}

// Клик по иконке удаления картинки (на выбранной превьюшке в модалке)
function setPreviewDeleteIconClick(preview) {
    deleteIcon = preview.find('a.itemPreviewDelete').first();
    $(deleteIcon).on('click', e => {
        preview.addClass('uk-hidden');
    });
}

// Собиралка картинок с галереи итема
function getItemImages() {
    let images = [];
    $('ul#itemImageSliderShow li').each((index, element) => {
        images.push({
            //fileType        : 'IMAGE',
            fileName        : $(element).attr('id').replace('image_', '').replace('_', '.'),
            itemId          : itemId,
            ownerLogin      : contentOwnerLogin
        });
    });
    return images;
}

// Вставлялка картинки в модалку
function addImageToModal(fileName, image) {
    let createdImagePreview = $($.parseHTML($('#blockImagePreviewShowTemplate').html())).clone();
    $('#modalItemImages div.uk-modal-body').append(createdImagePreview);
    createdImagePreview.attr('id', 'preview_' + fileName);
    setPreviewClick(createdImagePreview);
    setPreviewDeleteIconClick(createdImagePreview);
    createdImagePreview.find('img').first().attr('src', image);
}

// Собиралка удаленных (невидимых) картинок с модалки управления ими
function getDeletedImages() {
    let images = [];
    $('div.itemPreview.uk-hidden').each((index, element) => {
        images.push({
            //fileType        : 'IMAGE',
            fileName        : $(element).attr('id').replace('preview_', '').replace('_', '.'),
            ownerLogin      : contentOwnerLogin,
            itemId          : itemId
        });
    });
    return images;
}

// Загрузка картинки
function imageUpload(e) {
    const reader = new FileReader();
    let fileName;
    let canLoad = false;
    let itemId = $(e.currentTarget).attr('targetItem');
    reader.onload = file => {
        if (canLoad) {
            mimeType = reader.result.substring(reader.result.indexOf(':') + 1, reader.result.indexOf(';'));
            if ($.inArray(mimeType, consts.mimeTypes) == -1) {
                showError(messages.get('filetypeerror'));
            } else {
                //let savedFileName = uuidv4();
                //userFilePathArr
                const fileData = {
                    //fileType        : 'IMAGE',
                    //fileNewName     : savedFileName,
                    fileName        : uuidv4() + '.' + getFileExt(fileName),//fileName,
                    //fileMime        : mimeType,
                    //itemType        : getItemType(),
                    ownerLogin      : contentOwnerLogin,
                    itemId          : itemId,
                    fileBase64Body  : reader.result.substring(reader.result.indexOf(',') + 1)
                };
                modalLock($('#modalItemImages'), true);
                sendFile('../../' + getItemType().toLowerCase() + '/add/image', fileData, 'imagesuccessload', function() {imageLoadResult(true, fileData, reader.result);}, function() {imageLoadResult(false);});
            }
        }
    };
    if (e.target.files[0]) {
        reader.readAsDataURL(e.target.files[0]);
        fileName = e.target.files[0].name;
        canLoad = true;
    } else {
        canLoad = false;
        showError(messages.get('unknownerror'));
    }
}

// Реакция на ответ сервера о загрузке картинки
function imageLoadResult(success, fileData, base64Image) {
    if (success) {
       /* userFilePathArr = fileData.fileName.split('.');
        fileExt = userFilePathArr[userFilePathArr.length - 1];*/
        //fileExt = getFileExt(fileData.fileName);
        $('#inputItemImage').val('');
        let createdImagePreview = $($.parseHTML($('#blockImagePreviewShowTemplate').html())).clone();
        $('#modalItemImages div.uk-modal-body').append(createdImagePreview);
        createdImagePreview.attr('id', 'preview_' + fileData.fileName.replace('.', '_'));
        createdImagePreview.find('img').first().attr('src', base64Image);

        // Тут нужно установить onClick на createdImagePreview

        let createdImage = $($.parseHTML($('#blockImageMainShowTemplate').html())).clone();
        $('#itemImageSliderShow').append(createdImage);
        createdImage.attr('id', 'image_' + fileData.fileName.replace('.', '_'))
        createdImage.find('img').first().attr('src', base64Image);
        createdImage.find('a.lightBoxMember').first().attr('href', '../../picture/' + fileData.contentOwnerLogin + '/' + fileData.itemId + '/' + fileData.fileName);
        $('#itemImageContainer').removeClass('uk-hidden');
    }
    modalLock($('#modalItemImages'), false);
}

// Отправка запроса на удаление картинок
function sendQueryDeleteImage() {
    let deletedImages = getDeletedImages();
    if (deletedImages.length > 0) {
        $.ajax({
            url			: '../../' + getItemType().toLowerCase() + '/delete/image',//'../../file/delete',
            data		: JSON.stringify(deletedImages)// JSON.stringify(imageData)
            }).done(response => {
                $('div.itemPreview.uk-hidden').each((index, element) => {
                    $('li#' + $(element).attr('id').replace('preview_', 'image_')).remove();
                    $(element).remove();
                });
                closeModal();
            }).fail((response, status) => {
                showError(messages.get('unknownerror'));
            });
    } else {
        closeModal();
    }
}