$(document).ready(() => {
	setLabelEventHandlers();
});

function setLabelEventHandlers() {
// Приведение в первоначальный вид формы добавления новой метки перед ее показом
    UIkit.util.on('#modalLabelAdd', 'beforeshow', () => {
        clearForm('#formAddLabel');
        $('form#formAddLabel #newLabelColor').attr({'color':'GRAY', 'style':'color:GRAY'});
    });
// Клик по кнопке отправки формы добавления метки
    $('#buttonSaveNewLabel').off('click');
    $('#buttonSaveNewLabel').on('click', () => {
        sendFormAddLabel();
    });
// Клик по ссылке вызова модалки редактирования и добавления меток итема
    $('a.selectLabelSender').off('click');
    $('a.selectLabelSender').on('click', e => {
        senderModalId = $(e.currentTarget).closest('div.modal').attr('id');
        checkItemLabels(senderModalId);
        $('#modalLabelSelect .cancelLabelSelect').attr('href', '#' + senderModalId);
    });
// Клик по иконке с выбором цвета новой метки
    $('a.labelColorAdd').off('click');
    $('a.labelColorAdd').on('click', e => {
        setNewLabelColor($(e.currentTarget));
    });
// Клик по кнопке СОХРАНИТЬ на модадке редактуры меток
    $('button#labelSelect').off('click');
    $('button#labelSelect').on('click', e => {
        itemLabels = getCheckedLabels();
        setItemLabelsToForm($('#modalLabelSelect').find('button.uk-modal-close-default').attr('href'));
    });

    $('span.labelChecker').off('click');
    $('span.labelChecker').on('click', e => {
        checkbox = $(e.currentTarget).closest('div').find('input:checkbox');
        checkbox.prop('checked', !checkbox.is(':checked'));
    });
}

// Собиралка меток с карточки итема
function collectItemLabelsFromCard(editIcon) {
    cardLabelDiv = editIcon.closest('div.item').find('#itemLabels')
    itemLabels = new Object();
    $.each(cardLabelDiv.find('div'), (index, element) => {
        label = getLabelFromTemplate($(element));
        itemLabels[label.id] = label;
    });
}

// Собиралка объекта метки с меточного шаблона
function getLabelFromTemplate(templateDiv) {
    labelDataSpan = templateDiv.find('span.labelInfo').first();
    return {id:labelDataSpan.attr('labelId'), name:labelDataSpan.attr('labelName'), color:labelDataSpan.attr('labelColor')};
}

// Вставлялка меток итема в форму модалки add/edit итема
function setItemLabelsToForm(modalId) {
    targetLabelContainer = $(modalId).find('#labelContainer');
    targetLabelContainer.html('');
    for (key in itemLabels) {
        addedLabel = createLabelFromTemplate(itemLabels[key], false);
        addedLabel.appendTo(targetLabelContainer);
    }
}

// Собиралка отмеченных меток на форме их редактуры
function getCheckedLabels() {
    checkedLabels = new Object();
    $('#modalLabelSelect .uk-modal-body input:checkbox:checked').each((index, element) => {
        checkedLabel = getLabelFromTemplate($(element).closest('div'));
        checkedLabels[checkedLabel.id] = checkedLabel;
    });
    return checkedLabels;
}

// Установка цвета новой метки
function setNewLabelColor(colorHolder) {
    $('a.labelColorAdd').removeClass('uk-animation-fade');
    colorHolder.addClass('uk-animation-fade');
    $('#newLabelColor').css('color', colorHolder.attr('color'));
    $('#newLabelColor').attr('color', colorHolder.attr('color'));
}

// Id-шники меток итема
function getItemLabelIds() {
    return Object.keys(itemLabels);
}

// Прочекивалка существующих меток итема в модалке их редактирования
function checkItemLabels() {
    labelIds = getItemLabelIds();
    $('#modalLabelSelect .uk-modal-body input[type="checkbox"]').each((index, element) => {
        labelId = $(element).attr('id');
        $(element).prop('checked', labelIds.indexOf(labelId) > -1);
    });
}

// Отправка формы сохранения новой метки
function sendFormAddLabel() {
    labelName = $('#labelNameAdd').val();
    labelColor = $('#newLabelColor').attr('color');
    var validForm = true;
    if (isEmpty(labelName) || labelName.length < 3) {
        validForm = false;
        //setDangerInput('itemNameEdit');
    }
    if (!validForm) {
        showError(messages.get('incorrectsomedata'));
    } else {
        const labelData = {
            name    : labelName,
            color   : labelColor,
            type    : getItemType()
//            type    : pageTemplate.replace('Self', '').toUpperCase()
        };
        $.ajax({
            url			: '../../label/add',
            data		: JSON.stringify(labelData)
            }).done(response => {
                if (response.success) {
                    label = {id:response.message, name:labelName, color:labelColor};
                }
                jsonResponseResult(response, 'labelsuccessadd', function () {labelSuccessAdd(label);}, null);
            }).fail((response, status) => {
                showError(messages.get('unknownerror'));
            });
    }
}

// Успешное добавление новой метки
function labelSuccessAdd(label) {
    checkboxes = $('#modalLabelSelect .uk-modal-body input[type="checkbox"]');
    firstLabelName = getLabelFromTemplate(checkboxes.eq(0).closest('div')).name;
    lastLabelName = getLabelFromTemplate(checkboxes.eq(checkboxes.length - 1).closest('div')).name;
    if (firstLabelName.localeCompare(label.name) > 0) {
        checkboxes.eq(0).closest('div').before(createLabelFromTemplate(label, true));
    } else if (lastLabelName.localeCompare(label.name) < 0) {
        checkboxes.eq(checkboxes.length - 1).closest('div').after(createLabelFromTemplate(label, true));
    } else {
        for (var i = 0; i < checkboxes.length; i++) {
           labelName = getLabelFromTemplate(checkboxes.eq(i).closest('div')).name;
           nextLabelName = i < checkboxes.length - 1 ? nextLabelName = getLabelFromTemplate(checkboxes.eq(i + 1).closest('div')).name : null;
           if (labelName.localeCompare(label.name) <= 0 && (nextLabelName != null && nextLabelName.localeCompare(label.name) > 0)) {
               checkboxes.eq(i + 1).closest('div').before(createLabelFromTemplate(label, true));
               break;
           }
        }
    }
    itemLabels[label.id] = label;
    checkItemLabels();
    $('#modalLabelAdd button.uk-modal-close-default').trigger('click');
}

// Создавалка отображения метки по шаблону
function createLabelFromTemplate(labelObject, showCheckbox) {
    createdLabel = $($.parseHTML($('#blockLabelShowTemplate').html())).clone();
    checkbox = createdLabel.find('input[type="checkbox"]');
    if (showCheckbox) {
        checkbox.closest('span').removeClass('uk-hidden');
    } else {
        checkbox.closest('span').addClass('uk-hidden');
    }
    checkbox.attr('id', labelObject.id);
    createdLabel.find('span.labelIcon').attr('style', 'color:' + labelObject.color);
    createdLabel.find('span.labelText').text(labelObject.name);
    createdLabel.find('span.labelInfo').attr({'labelId':labelObject.id, 'labelName':labelObject.name, 'labelColor':labelObject.color});
    return createdLabel;
}