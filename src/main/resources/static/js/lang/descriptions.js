﻿descriptions = new Map([
    ['main'            ,   'Description главной страницы'],
    ['profile'         ,   'Description страницы профиля пользователя'],
    ['aquariumself'    ,   'Description страницы своих аквариумов'],
    ['aquariumuser'    ,   'Description страницы аквариумов другого юзера'],
    ['aquariumitem'    ,   'Description страницы аквариума']
]);