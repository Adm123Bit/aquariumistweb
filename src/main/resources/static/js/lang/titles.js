﻿titles = new Map([
    ['main'            ,   'Тайтл главной страницы'],
    ['profile'         ,   'Title страницы пользователя'],
    ['aquariumself'    ,   'Title страницы своих аквариумов'],
    ['aquariumuser'    ,   'Title страницы аквариумов другого юзера'],
    ['aquariumitem'    ,   'Title страницы аквариума']
]);