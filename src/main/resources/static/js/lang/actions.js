﻿actions = {
    yes                 : 'Да',
    no                  : 'Нет',
    save                : 'Сохранить',
    cancel              : 'Отмена',
    add                 : 'Добавить',
    send                : 'Отправить',
    deleteselect        : 'Удалить отмеченные',
    archiveselect       : 'Архивировать отмеченные',
    unarchiveselect     : 'Восстановить отмеченные'
}