messages = new Map([
    ['passworddifference'   ,   'Введенные пароли различаются...'],
    ['incorrectsomedata'    ,   'Не все данные указны корректно...'],
    ['filetypeerror'        ,   'Недопустимый тип файла...'],
    ['filetoolarge'         ,   'Файл слишком велик...'],
    ['avatarchanged'        ,   'Аватарка успешно изменена'],
    ['accountcreate'        ,   'Ваш аккаунт успешно создан'],
    ['passwordchanged'      ,   'Пароль успешно изменен'],
    ['successadd'           ,   'Запись успешно добавлена'],
    ['labelsuccessadd'      ,   'Метка успешно добавлена'],
    ['successdelete'        ,   'Запись успешно удалена'],
    ['successedit'          ,   'Запись успешно изменена'],
    ['successunarchive'     ,   'Запись успешно восстановлена'],
    ['imagesuccessload'     ,   'Изображение успешно добавлено'],
    ['successmultidelete'   ,   'Записи успешно удалены'],
    ['successmultiarchive'  ,   'Записи успешно заархивированы'],
    ['successmultiunarchive',   'Записи успешно восстановлены'],
    ['usernotfound'         ,   'Увы, такой пользователь не найден...'],
    ['useralwaysexists'     ,   'К сожалению этот логин уже занят...'],
    ['incorrectpassword'    ,   'Пароль указан некорректно...'],
    ['incorrectlogin'       ,   'Логин указан некорректно...'],
    ['unknownerror'         ,   'Что-то пошло не так и в итоге ничего не получилось...'],
    ['youhavenotrights'     ,   'У Вас нет прав на на это действие...']
]);