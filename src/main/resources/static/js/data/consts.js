﻿consts = {
    mimeTypes       :   ['image/bmp', 'image/jpeg', 'image/x-icon', 'image/gif', 'image/png'],
    maxAvatarSize   :   512000
}