$.ajaxSetup({
	type		: 'post',
    contentType : 'application/json',
	beforeSend	: function() {
	    modal = getCurrentModal();
	    modalLock(modal, true);
	},
	complete	: function() {
	    modal = getCurrentModal();
	    modalLock(modal, false);
	}
});

$(document).ready(() => {
	setCommonEventHandlers();
    setPageTitleAndDescription();
});

function setPageTitleAndDescription() {
    $('title').text(titles.get(pageTemplate.toLowerCase()));
    $('meta[name = Description]').attr('content', descriptions.get(pageTemplate.toLowerCase()));
}

// Общие слухачи событий и предустановки
function setCommonEventHandlers() {
    if (!flashPlayerInstalled()) {
        $('#bubbles').addClass('uk-hidden');
    }
// Очищалка красных рамок на инпутах при вводе
/*    $('input').off('input');
    $('input').on('input', e => {
        $(e.currentTarget).removeClass('uk-form-danger');
    });*/
    pageInfoTag = $('div#pageInfo');
    pageTemplate = pageInfoTag.attr('pageTemplate');
    contentOwnerLogin = pageInfoTag.attr('contentOwnerLogin');
    itemId = pageInfoTag.attr('itemId');

/*    if (contentOwnerLogin == undefined) {
        contentOwnerLogin = null;
    }*/
    setMaxDateToday();
    setButtonsText();
}

// Установка макс. даты на сегодня в соответствующие инпуты
function setMaxDateToday() {
    attrStr = nowDateString();
    $('input.maxToday').attr('max', attrStr);
}

// Надписи на кнопках форрм
function setButtonsText() {
    $('.actionYes').text(actions.yes);
    $('.actionNo').text(actions.no);
    $('.actionCancel').text(actions.cancel);
    $('.actionSave').text(actions.save);
    $('.actionAdd').text(actions.add);
    $('.actionSend').text(actions.send);
    $('.actionDeleteSelect').text(actions.deleteselect);
    $('.actionArchiveSelect').text(actions.archiveselect);
    $('.actionUnArchiveSelect').text(actions.unarchiveselect);
}

// Отправка файла
function sendFile(destController, fileData, successMessage, successCallBack, errorCallBack) {
/*    const fileData = {
        fileType        : fileType,
        fileName        : fileName,
        fileMime        : mimeType,
        fileBase64Body  : base64String
    };*/
    $.ajax({
        url			: controller,
        data		: JSON.stringify(fileData),
        dataType	: 'json'
    }).done(response => {
        jsonResponseResult(response, successMessage, successCallBack, errorCallBack);
    }).fail((response, status) => {
        showError(messages.get('unknownerror'));
        if (errorCallBack != null) {
            errorCallBack();
        }
    });
}

// Обновление DOM-а после ответа сервера
function refreshDOM(response) {
    responseDOM = $($.parseHTML(response));
    pageTop = responseDOM.find('#pageTop').first();
    $('#pageTop').html(pageTop.html());
    pageContent = responseDOM.find("#pageContent").first();
    $('#pageContent').html(pageContent.html());
    pageRight = responseDOM.find("#pageRight").first();
    $('#pageRight').html(pageRight.html());
    setModal(responseDOM, 'modalItemAdd');
    setModal(responseDOM, 'modalItemEdit');
    setModal(responseDOM, 'modalItemDelete');
    setModal(responseDOM, 'modalItemUnarchive');
    setModal(responseDOM, 'modalLabelSelect');
    setModal(responseDOM, 'modalLabelAdd');
    setModal(responseDOM, 'modalConfirmClose');
    setModal(responseDOM, 'modalConfirmMultiDelete');
    setModal(responseDOM, 'modalConfirmMultiArchive');
    setModal(responseDOM, 'modalConfirmMultiUnarchive');
    setModal(responseDOM, 'modalItemImages');
    setCommonEventHandlers();
    setAuthorizationEventHandlers();
    if (pageTemplate == 'Profile') {
        setProfileEditEventHandlers();
    }
    if (pageTemplate.endsWith('Self') || pageTemplate.endsWith('Item')) {
        setItemEventHandlers();
        setLabelEventHandlers();
        if (pageTemplate.endsWith('Item')) {
            setImageEventHandlers();
        }
    }
}

// Вставлялка-удалялка модалок, в зависимости от ответа сервера
function setModal(responseDOM, modalId) {
    let modal = responseDOM.find('#' + modalId).first();
    if ($('div').is('#' + modalId)) {
        if (modal.length == 0) {
            let currentModal = getCurrentModal();
            $('#' + modalId).remove();
        }
    } else {
        if (modal.length != 0) {
            $('body').append(modal);
        }
    }
}

// Разбор ответа сервера, когда он присылает DtoActionResult
function jsonResponseResult(response, successMessage, successCallBack, errorCallBack) {
    if (response.success) {
        if (!isEmpty(successMessage)) {
            showSuccess(messages.get(successMessage));
        }
        if (successCallBack != null) {
            successCallBack();
        }
    } else {
        showError(messages.get(isEmpty(response.message) ? 'unknownerror' : response.message));
        if (errorCallBack != null) {
            errorCallBack();
        }
    }
}

// Показ сообщения об ошибке при 400 отете сервера (данные не прошли валидацию)
function responseFail(response) {
    errorMessageShow = false;
    $.each(JSON.parse(response.responseText).errors, (index, erroritem) => {
        errorMessage = messages.get(erroritem.defaultMessage);
        if (errorMessage != undefined) {
            showError(errorMessage);
            errorMessageShow = true;
        }
    });
    if (!errorMessageShow) {
        showError(messages.get('unknownerror'));
    }
}

// Очистка формы
function clearForm(form) {
	$(form)[0].reset();
}

// Показ всплывающего сообщения об ошибке
function showError(errorMessage) {
    UIkit.notification({
        message: errorMessage, status: 'danger'
    });
}

// Показ всплывающего сообщения об успехе
function showSuccess(successMessage) {
    UIkit.notification({
        message: successMessage, status: 'success'
    });
}

// Отыскивалка открытой модалки
function getCurrentModal() {
    return $('div.uk-modal.uk-flex.uk-open').first();
}

// Закрытие модалки
function closeModal() {
    //UIkit.modal('#' + modalId).hide();
    let modal = getCurrentModal();
    modalLock(modal, false);
    //console.log(modal.attr('id'));
    UIkit.modal('#' + modal.attr('id')).hide();
}

// Лочилка модалки на время отправки
function modalLock(modal, isLock) {
    if (isLock) {
        modal.find('div.waitSpinner').removeClass('uk-hidden');
    } else {
        modal.find('div.waitSpinner').addClass('uk-hidden');
    }
    /*modal.find('button').prop('disabled', isLock);
    modal.find('a').prop('disabled', isLock);
    modal.find('input').prop('disabled', isLock);*/
}

// Установка красных границ для инпута
function setDangerInput(id) {
    $('#' + id).addClass('uk-form-danger');
}

/*// Очистка красных границ для инпута
function clearDangerInput(id) {
    $('#' + id).removeClass('uk-form-danger');
}*/

// Проверка строки на пустоту
function isEmpty(str) {
    if (str === null || str === 'undefined' || str.trim() === '') {
        return true;
    }
    return false;
}

// Текущая дата больше, меньше, равна указанной в аргументе
function compareDates(dateString) {
    now = new Date();
    now.setHours(0, 0, 0, 0);
    comparableDate = Date.parse(dateString + 'T00:00:00.000Z');
    if (now == comparableDate) {
        return 0;
    } else {
        return comparableDate > now ? 1 : -1;
    }
}

// Строка yyyy-MM-dd из текущей даты
function nowDateString() {
    var date = new Date();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    return date.getFullYear() + '-' + (month < 10 ? '0' : '') + month + '-' + (day < 10 ? '0' : '') + day;
}

// Первый символ строки - в верхний регистр
function ucFirst(str) {
    if (!str) return str;
    return str[0].toUpperCase() + str.slice(1);
}

// Генератор UUID
function uuidv4() {
    var d = new Date().getTime();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
        d += performance.now(); //use high-precision timer if available
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}

//Расширение файла
function getFileExt(fileName) {
    userFilePathArr = fileName.split('.');
    fileExt = userFilePathArr[userFilePathArr.length - 1];
    return userFilePathArr.length > 1 ? fileExt : '';
}

function flashPlayerInstalled() {
    flashInstalled = false;
    if (typeof(navigator.plugins) != 'undefined' && typeof(navigator.plugins['Shockwave Flash']) == 'object') {
        flashInstalled = true;
    } else if (typeof window.ActiveXObject != 'undefined') {
        try {
            if (new ActiveXObject('ShockwaveFlash.ShockwaveFlash')) {
                flashInstalled = true;
            }
        } catch(e) {};
    }
    return flashInstalled;
}