$(document).ready(() => {
	setItemEventHandlers();
	checkedItems = [];
});

function setItemEventHandlers() {
// клик по ссылке вызова модалки добавления итема
    $('a.addItemModalSender').off('click');
    $('a.addItemModalSender').on('click', () => {
        itemLabels = new Object();
        setItemLabelsToForm('#modalItemAdd');
    });
// Клик по иконке удаления на карточке итема (Установка id итема в атрибут кнопки ДА модалки подтверждения удаления)
    $('a.deleteItem').off('click');
    $('a.deleteItem').on('click', e => {
    //console.log(getClickedItemId(e));
        //$('#buttonDelete').attr('item', $(e.currentTarget).closest('div').attr('item'));
        $('#buttonDelete').attr('item', getClickedItemId(e));
    });
// Клик по иконке разархивации на карточке итема (Установка id итема в атрибут кнопки ДА модалки подтверждения разархивации)
    $('a.unArchiveItem').off('click');
    $('a.unArchiveItem').on('click', e => {
        $('#buttonUnArchive').attr('item', getClickedItemId(e));
    });
// Клик по иконке редактирования на карточке итема (Установка id и данных редактируемого итема в форму редактирования)
    $('a.editItem').off('click');
    $('a.editItem').on('click', e => {
        collectItemDataFromCard($(e.currentTarget));
        collectItemLabelsFromCard($(e.currentTarget));
        setItemLabelsToForm('#modalItemEdit');
    });
// клик по кнопке редактирования картинок на карточке итема
    $('a.addImageItem').off('click');
    $('a.addImageItem').on('click', e => {
        $('#inputItemImage').attr('targetItem', getClickedItemId(e));
    });
// Клик по иконке "отметить" на карточке итема
    $('a.checkItem').off('click');
    $('a.checkItem').on('click', e => {
        clickedItemId = getClickedItemId(e);
        itemIndexInCheckedArr = checkedItems.indexOf(clickedItemId);
        if (itemIndexInCheckedArr == -1) {
            checkedItems.push(clickedItemId);
            $(e.currentTarget).css('color', 'GREEN');
        } else {
            checkedItems.splice(itemIndexInCheckedArr, 1);
            $(e.currentTarget).css('color', '');
        }
        existChecked = checkedItems.length > 0;
        if (existChecked) {
            $('a#actionArchiveSelect').attr('href', '#modalConfirmMultiArchive');
            $('a#actionUnArchiveSelect').attr('href', '#modalConfirmMultiUnarchive');
            $('a#actionDeleteSelect').attr('href', '#modalConfirmMultiDelete');
            $('a#actionArchiveSelect').removeClass('uk-link-muted');
            $('a#actionUnArchiveSelect').removeClass('uk-link-muted');
            $('a#actionDeleteSelect').removeClass('uk-link-muted');
            $('a#actionArchiveSelect').attr('uk-toggle', '');
            $('a#actionUnArchiveSelect').attr('uk-toggle', '');
            $('a#actionDeleteSelect').attr('uk-toggle', '');
        } else {
            $('a#actionArchiveSelect').removeAttr('href');
            $('a#actionUnArchiveSelect').removeAttr('href');
            $('a#actionDeleteSelect').removeAttr('href');
            $('a#actionArchiveSelect').addClass('uk-link-muted');
            $('a#actionUnArchiveSelect').addClass('uk-link-muted');
            $('a#actionDeleteSelect').addClass('uk-link-muted');
            $('a#actionArchiveSelect').removeAttr('uk-toggle');
            $('a#actionUnArchiveSelect').removeAttr('uk-toggle');
            $('a#actionDeleteSelect').removeAttr('uk-toggle');
        }
    });
// Клик по кнопке ДА модалки удаления отмеченных итемов
    $('#multiDelete').off('click');
    $('#multiDelete').on('click', () => {
        sendQueryMultiItemAction('delete');
    });
// Клик по кнопке ДА модалки архивации отмеченных итемов
    $('#multiArchive').off('click');
    $('#multiArchive').on('click', () => {
        sendQueryMultiItemAction('archive');
    });
// Клик по кнопке ДА модалки разархивации отмеченных итемов
    $('#multiUnArchive').off('click');
    $('#multiUnArchive').on('click', () => {
        sendQueryMultiItemAction('unarchive');
    });
// Клик по кнопке отправки формы добавления итема
    $('#buttonSaveNewItem').off('click');
    $('#buttonSaveNewItem').on('click', () => {
        sendFormAddItem();
    });
// Клик по кнопке отправки формы редактирования итема
    $('#buttonEditItem').off('click');
    $('#buttonEditItem').on('click', () => {
        sendFormEditItem();
    });
// Клик по кнопке ДА модалки подтверждения удаления
    $('#buttonDelete').off('click');
    $('#buttonDelete').on('click', () => {
        sendQueryDeleteItem();
    });
// Клик по кнопке ДА модалки подтверждения разархивации
    $('#buttonUnArchive').off('click');
    $('#buttonUnArchive').on('click', () => {
        sendQueryUnArchiveItem();
    });
// Установка модалки, к которой нужно вернуться при нажатии НЕТ в модалке с запросом об отмене add/edit
    $('button.confirmCloseSender').off('click');
    $('button.confirmCloseSender').on('click', e => {
        $('#modalConfirmClose .cancelCloseAction').attr('href', '#' + $(e.currentTarget).closest('div.modal').attr('id'));
    });
/*
// Клик по кнопке добавки картинки к итему в модалке редактуры картинок (вызов диалога)
    $('#iemImageAdd').off('click');
    $('#iemImageAdd').on('click', () => {
        $('#inputItemImage').trigger('click');
    });
// Слушалка изменения содержимого инпута новой картинки
    $('#inputItemImage').off('click');
    $('#inputItemImage').on('change', e => {
        imageUpload(e);
    });
// Клик по превьюшке в модалке управления картинками итема
    $('div.itemPreview').off('click');
    $('div.itemPreview').on('click', e => {
        previewCover = $(e.currentTarget).find('div.itemPreviewCover');
        previewCover.toggleClass('uk-hidden');
    });
// Клик по иконке удаления картинки (на выбранной превьюшке в модалке)
    $('a.itemPreviewDelete').off('click');
    $('a.itemPreviewDelete').on('click', e => {
    });
*/
}

// Определялка id итема, по иконке на карточке которого клик
function getClickedItemId(clickEvent) {
    return $(clickEvent.currentTarget).closest('div.item').find('span.dtoData').first().attr('id');
}

// Отправка запроса на удаление итема
function sendQueryDeleteItem() {
    const aquariumData = {
        id          : $('#buttonDelete').attr('item'),
        pageTemplate: pageTemplate
    };
    $.ajax({
        url			: '../../' + getItemType() + '/delete',
        data		: JSON.stringify(aquariumData)
        }).done(response => {
            showSuccess(messages.get('successdelete'));
            refreshDOM(response);
            closeModal();
        }).fail((response, status) => {
            showError(messages.get('unknownerror'));
        });
}

// Отправка запроса на разархивацию итема
function sendQueryUnArchiveItem() {
    const aquariumData = {
        id          : $('#buttonUnArchive').attr('item'),
        pageTemplate: pageTemplate
    };
    $.ajax({
        url			: '../../' + getItemType() + '/unarchive/',
        data		: JSON.stringify(aquariumData)
        }).done(response => {
            showSuccess(messages.get('successunarchive'));
            refreshDOM(response);
            closeModal();
        }).fail((response, status) => {
            showError(messages.get('unknownerror'));
        });
}

// Отправка запроса на действие с отмеченными итемами
function sendQueryMultiItemAction(action) {
    if (checkedItems.length > 0) {
    const itemMultiData = {
        pageTemplate : pageTemplate,
        itemIds     : checkedItems
    }
    $.ajax({
        url			: '../../' + getItemType() + '/' + action + '/selected',
        data		: JSON.stringify(itemMultiData)
        }).done(response => {
            showSuccess(messages.get('successmulti' + action));
            refreshDOM(response);
            checkedItems = [];
            closeModal(/*'modalConfirmMulti' + ucFirst(action)*/);
        }).fail((response, status) => {
            showError(messages.get('unknownerror'));
        });
    }
}

// Определялка типа итема по имени шаблона
function getItemType() {
    return pageTemplate.replace('Self', '').replace('Item', '').toUpperCase();
}

// Загрузка картинки
function imageUpload(e) {
    const reader = new FileReader();
    let fileName;
    let canLoad = false;
    let itemId = $(e.currentTarget).attr('targetItem');
    reader.onload = file => {
        if (canLoad) {
            mimeType = reader.result.substring(reader.result.indexOf(':') + 1, reader.result.indexOf(';'));
            if ($.inArray(mimeType, consts.mimeTypes) == -1) {
                showError(messages.get('filetypeerror'));
            } else {
                const fileData = {
                    //fileType        : 'itemImage',
                    fileName        : fileName,
                    //fileMime        : mimeType,
                    itemType        : getItemType(),
                    itemId          : itemId,
                    fileBase64Body  : reader.result.substring(reader.result.indexOf(',') + 1)
                };
                modalLock($('#modalItemImages'), true);
                sendFile(fileData, 'imagesuccessload', function() {imageLoadResult(true, reader.result);}, function() {imageLoadResult(false);});
            }
        }
    };
    if (e.target.files[0]) {
        reader.readAsDataURL(e.target.files[0]);
        fileName = e.target.files[0].name;
        canLoad = true;
    } else {
        canLoad = false;
        showError(messages.get('unknownerror'));
    }
}

// Реакция на ответ сервера о загрузке картинки
function imageLoadResult(success, base64Avatar) {
    if (success) {
        $('#inputItemImage').val('');
        // Здесь отобразить картинку
    }
    modalLock($('#modalItemImages'), false);
}