$(document).ready(() => {
	setProfileEditEventHandlers();
});

function setProfileEditEventHandlers() {
// Слушалка изменения файла автарки
    $('#inputAvatar').off('click');
    $('#inputAvatar').on('change', e => {
        avatarUpload(e);
    });
// Клик по кнопке отправки формы изменения пароля
    $('#buttonEditPassword').off('click');
    $('#buttonEditPassword').on('click', () => {
        sendFormEditPassword();
    });
}

// Показывалка крутилки на аватаре
function avatarLoaderIndicator(show) {
    show ? $('#avatarLoaderIndicator').removeClass('uk-hidden') : $('#avatarLoaderIndicator').addClass('uk-hidden');
}

// Реакция на ответ сервера о загрузке аватарки
function avatarLoadResult(success, base64Avatar) {
    if (success) {
        $('#userAvatar').attr('src', base64Avatar);
    }
    avatarLoaderIndicator(false);
}

// Загрузка аватарки
function avatarUpload(e) {
    const reader = new FileReader();
    let fileName;
    let canLoad = false;
    reader.onload = file => {
        if (canLoad) {
            mimeType = reader.result.substring(reader.result.indexOf(':') + 1, reader.result.indexOf(';'));
            if ($.inArray(mimeType, consts.mimeTypes) == -1) {
                showError(messages.get('filetypeerror'));
            } else {
                const fileData = {
                    //fileType        : 'AVATAR',
                    //fileNewName     : 'avatar',
                    fileName        : 'avatar.' + getFileExt(fileName),//fileName,
                    //fileMime        : mimeType,
                    fileBase64Body  : reader.result.substring(reader.result.indexOf(',') + 1)
                };
                avatarLoaderIndicator(true);
                sendFile('../../profile/edit/avatar', fileData, 'avatarchanged', function() {avatarLoadResult(true, reader.result);}, function() {avatarLoadResult(false);});
            }
        }
    };
    if (e.target.files[0]) {
        if (e.target.files[0].size <= consts.maxAvatarSize) {
            reader.readAsDataURL(e.target.files[0]);
            fileName = e.target.files[0].name;
            canLoad = true;
        } else {
            showError(messages.get('filetoolarge'));
        }
    } else {
        canLoad = false;
        showError(messages.get('unknownerror'));
    }
}

// Отправка формы изменения пароля
function sendFormEditPassword() {
    userPass1 = $('#passwordForEditPassword').val();
    userPass2 = $('#passwordConfirmForEditPassword').val();
    validForm = true;
    if (userPass1 != userPass2) {
        showError(messages.get('passworddifference'));
        validForm = false;
    }
    if (!passwordRegExp.test(userPass1)) {
        showError(messages.get('incorrectpassword'));
        validForm = false;
    }
    const passwordData = {
        password        : userPass1
    };
    if (validForm) {
        $.ajax({
            url			: '../../profile/edit/password',
            data		: JSON.stringify(passwordData)
        }).done(response => {
            jsonResponseResult(response, 'passwordchanged', null, null);
        }).fail((response, status) => {
            responseFail(response);
            //showError(messages.get('unknownerror'));
        });
	}
}
