package ru.adm123.aquariumist.enums;

public enum LabelColor {

    GRAY,
    BROWN,
    BLUE,
    GREEN,
    RED,
    YELLOW

}
