package ru.adm123.aquariumist.enums;

public enum ItemType {

    AQUARIUM,
    DEVICE,
    PET,
    PLANT,
    NOTE,
    TASK,
    EXPERIMENT

}
