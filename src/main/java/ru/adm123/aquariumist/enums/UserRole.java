package ru.adm123.aquariumist.enums;

public enum UserRole {

    ADMIN,
    USER,
    GUEST

}
