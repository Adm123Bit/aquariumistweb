package ru.adm123.aquariumist.domain.entity;

import com.vladmihalcea.hibernate.type.basic.PostgreSQLEnumType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import ru.adm123.aquariumist.enums.ItemShare;
import ru.adm123.aquariumist.enums.ItemType;
import ru.adm123.aquariumist.enums.LabelColor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.UUID;

@Setter
@Getter
@TypeDef(name = "pgsql_enum", typeClass = PostgreSQLEnumType.class)
@Entity
@NoArgsConstructor
@Table(name = "labels")
public class EntityLabel {

    @Column(name = "id")
    @Id
    @GeneratedValue(generator = "generatorUUID")
    @GenericGenerator(name = "generatorUUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner", referencedColumnName = "id")
    private EntityUser owner;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    @Type(type = "pgsql_enum")
    private ItemType type;

    @Column(name = "name")
    private String name;

    @Column(name = "color")
    @Enumerated(EnumType.STRING)
    @Type(type = "pgsql_enum")
    private LabelColor color;

    @Column(name = "delete")
    private boolean deleted;

    @Column(name = "add")
    private LocalDateTime addDateTime;

    @Column(name = "edit")
    private LocalDateTime editDateTime;

/*    @ManyToMany(mappedBy = "labels")
    private Collection<EntityItemAbstract> items;*/

}