package ru.adm123.aquariumist.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Setter
@Getter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user_session")
public class EntitySession {

    @Id
    @Column(name = "session_id")
    private UUID sessionId;

    @Column(name = "user_id")
    private Long userId;

}
