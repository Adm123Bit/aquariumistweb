package ru.adm123.aquariumist.domain.entity;

import com.vladmihalcea.hibernate.type.basic.PostgreSQLEnumType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import ru.adm123.aquariumist.enums.ItemShare;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Setter
@Getter
@TypeDef(name = "pgsql_enum", typeClass = PostgreSQLEnumType.class)
@Entity
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "items")
public abstract class EntityItemAbstract {

    @Column(name = "id")
    @Id
    @GeneratedValue(generator = "generatorUUID")
    @GenericGenerator(name = "generatorUUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner", referencedColumnName = "id")
    private EntityUser owner;

    @Column(name = "share")
    @Enumerated(EnumType.STRING)
    @Type(type = "pgsql_enum")
    private ItemShare share;

    @Column(name = "archive")
    private boolean archived;

    @Column(name = "delete")
    private boolean deleted;

    @Column(name = "add")
    private LocalDateTime addDateTime;

    @Column(name = "edit")
    private LocalDateTime editDateTime;

    @ManyToMany
    @JoinTable(name = "item_labels",
            joinColumns = @JoinColumn(name = "item_id"),
            inverseJoinColumns = @JoinColumn(name = "id"))
    private Collection<EntityLabel> labels;

}