package ru.adm123.aquariumist.domain.entity;

import com.vladmihalcea.hibernate.type.basic.PostgreSQLEnumType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import ru.adm123.aquariumist.enums.UserRole;

import javax.persistence.*;

@Setter
@Getter
@TypeDef(name = "pgsql_enum", typeClass = PostgreSQLEnumType.class)
@Entity
@NoArgsConstructor
@Table(name = "user_data")
public class EntityUser extends EntityUserAbstract {

    @Column(name = "role")
    @Enumerated(EnumType.STRING)
    @Type(type = "pgsql_enum")
    private UserRole role;

}
