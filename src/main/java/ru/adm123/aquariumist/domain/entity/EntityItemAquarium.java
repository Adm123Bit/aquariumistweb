package ru.adm123.aquariumist.domain.entity;

import com.vladmihalcea.hibernate.type.basic.PostgreSQLEnumType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@TypeDef(name = "pgsql_enum", typeClass = PostgreSQLEnumType.class)
@Entity
@NoArgsConstructor
@Table(name = "aquariums")
public class EntityItemAquarium extends EntityItemAbstract {

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "volume")
    private Integer volume;

    @Column(name = "start")
    private LocalDate startDate;

}