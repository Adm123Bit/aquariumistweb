package ru.adm123.aquariumist.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public abstract class DtoUserAbstract {

    private Long id;
    private String login;
    private String password;

}
