package ru.adm123.aquariumist.domain.dto.form;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Setter
@Getter
public class DtoFormFile {

    private String fileName;
    //private String fileNewName;
    //private FileType fileType;
    //private String fileMime;
    private String fileBase64Body;
    private String ownerLogin;
    //private ItemType itemType;
    private UUID itemId;

}
