package ru.adm123.aquariumist.domain.dto.form;

import lombok.Getter;
import lombok.Setter;
import ru.adm123.aquariumist.validator.annotation.CheckUserExists;
import ru.adm123.aquariumist.validator.impl.UserExistByLoginAndPassword;

@Setter
@Getter
@CheckUserExists(objectValidator = UserExistByLoginAndPassword.class, message = "usernotfound")
public class DtoFormUserLogin extends DtoFormContentUpdateAbstract {

    private String login;
    private String password;
    private boolean check;

}
