package ru.adm123.aquariumist.domain.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class DtoItemAquarium extends DtoItemAbstract {

    private String name;
    private String description;
    private Integer volume;
    //private String startDate;
    private LocalDate startDate;

}
