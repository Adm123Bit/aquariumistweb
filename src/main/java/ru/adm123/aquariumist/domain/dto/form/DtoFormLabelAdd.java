package ru.adm123.aquariumist.domain.dto.form;

import lombok.Getter;
import lombok.Setter;
import ru.adm123.aquariumist.enums.ItemType;
import ru.adm123.aquariumist.enums.LabelColor;
import ru.adm123.aquariumist.validator.annotation.CheckCurrentUser;

@Setter
@Getter
@CheckCurrentUser(message = "youhavenotrights")
public class DtoFormLabelAdd {

    private String name;
    private ItemType type;
    private LabelColor color;

}
