package ru.adm123.aquariumist.domain.dto;

import lombok.*;
import ru.adm123.aquariumist.domain.entity.EntityItemAbstract;
import ru.adm123.aquariumist.domain.entity.EntityUser;
import ru.adm123.aquariumist.enums.ItemType;
import ru.adm123.aquariumist.enums.LabelColor;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
public class DtoLabel implements Comparable<DtoLabel> {

    private UUID id;
    private DtoUser owner;
    private ItemType type;
    private String name;
    private LabelColor color;
    private boolean deleted;
    private LocalDateTime addDateTime;
    private LocalDateTime editDateTime;

    @Override
    public int compareTo(DtoLabel obj) {
        if (obj == null) {
            return -1;
        }
        return this.name.compareTo(obj.name);
    }

}