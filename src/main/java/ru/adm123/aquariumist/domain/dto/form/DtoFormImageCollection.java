package ru.adm123.aquariumist.domain.dto.form;

import lombok.Getter;
import lombok.Setter;
import ru.adm123.aquariumist.enums.ItemType;

import java.util.Collection;
import java.util.UUID;

@Setter
@Getter
public class DtoFormImageCollection {

    private Collection<String> imageFileNames;
    private UUID itemId;

}
