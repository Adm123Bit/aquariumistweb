package ru.adm123.aquariumist.domain.dto;

import lombok.Getter;
import lombok.Setter;
import ru.adm123.aquariumist.enums.UserRole;

@Setter
@Getter
public class DtoUser extends DtoUserAbstract {

    private UserRole role;
    private String avatar;

}
