package ru.adm123.aquariumist.domain.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class DtoItemTypeCard {

    private String typeName;
    private String name;
    private String description;
    private String iconFile;

}
