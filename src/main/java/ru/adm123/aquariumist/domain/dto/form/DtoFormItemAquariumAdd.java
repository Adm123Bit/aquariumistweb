package ru.adm123.aquariumist.domain.dto.form;

import lombok.Getter;
import lombok.Setter;
import ru.adm123.aquariumist.validator.annotation.CheckCurrentUser;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Setter
@Getter
@CheckCurrentUser(message = "youhavenotrights")
public class DtoFormItemAquariumAdd extends DtoFormItemAbstract {

    private String name;
    private String description;
    private Integer volume;
    private LocalDate startDate;

}
