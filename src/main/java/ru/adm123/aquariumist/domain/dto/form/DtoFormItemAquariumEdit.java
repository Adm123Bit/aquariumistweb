package ru.adm123.aquariumist.domain.dto.form;

import lombok.Getter;
import lombok.Setter;
import ru.adm123.aquariumist.validator.annotation.CheckCurrentUser;
import ru.adm123.aquariumist.validator.annotation.CheckItemExists;
import ru.adm123.aquariumist.validator.impl.AquariumExistById;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Setter
@Getter
@CheckCurrentUser(message = "youhavenotrights")
@CheckItemExists(objectValidator = AquariumExistById.class, message = "unknownerror")
public class DtoFormItemAquariumEdit extends DtoFormItemAbstract {

    private Integer volume;
    private LocalDate startDate;

}
