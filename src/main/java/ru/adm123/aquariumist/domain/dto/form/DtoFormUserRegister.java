package ru.adm123.aquariumist.domain.dto.form;

import lombok.Getter;
import lombok.Setter;
import ru.adm123.aquariumist.validator.annotation.CheckUserExists;
import ru.adm123.aquariumist.validator.impl.UserNotExistByLogin;

@Setter
@Getter
@CheckUserExists(objectValidator = UserNotExistByLogin.class, message = "useralwaysexists")
public class DtoFormUserRegister {

    private String login;
    private String password;

}
