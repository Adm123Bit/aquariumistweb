package ru.adm123.aquariumist.domain.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.adm123.aquariumist.enums.ItemShare;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.TreeSet;
import java.util.UUID;

@Setter
@Getter
public abstract class DtoItemAbstract {

    private UUID id;
    private DtoUser owner;
    private ItemShare share;
    private boolean archived;
    private boolean deleted;
    private LocalDateTime addDateTime;
    private LocalDateTime editDateTime;
    private TreeSet<DtoLabel> labels;
    private String cover;

}
