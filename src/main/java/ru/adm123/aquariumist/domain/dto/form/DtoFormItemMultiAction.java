package ru.adm123.aquariumist.domain.dto.form;

import lombok.Getter;
import lombok.Setter;

import java.util.Collection;
import java.util.UUID;

@Setter
@Getter
public class DtoFormItemMultiAction {

    private String pageTemplate;
    private Collection<UUID> itemIds;

}
