package ru.adm123.aquariumist.domain.dto.form;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Setter
@Getter
public abstract class DtoFormContentUpdateAbstract {

    private String pageTemplate;
    private String contentOwnerLogin;
    private UUID itemId;

}
