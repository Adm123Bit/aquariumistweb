package ru.adm123.aquariumist.domain.dto.form;

import lombok.Getter;
import lombok.Setter;
import ru.adm123.aquariumist.enums.ItemShare;

import java.util.List;
import java.util.UUID;

@Setter
@Getter
public abstract class DtoFormItemAbstract {

    private UUID id;
    private String name;
    private String description;
    private ItemShare share;
    private List<UUID> labelIds;
    private boolean archived;
    private boolean deleted;
    private String pageTemplate;

}