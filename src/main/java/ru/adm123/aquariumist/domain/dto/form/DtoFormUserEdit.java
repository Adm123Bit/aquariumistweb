package ru.adm123.aquariumist.domain.dto.form;

import lombok.Getter;
import lombok.Setter;
import ru.adm123.aquariumist.validator.annotation.CheckCurrentUser;

@Setter
@Getter
@CheckCurrentUser(message = "youhavenotrights")
public class DtoFormUserEdit {

    private String password;

}
