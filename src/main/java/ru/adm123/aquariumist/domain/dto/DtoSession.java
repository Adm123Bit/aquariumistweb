package ru.adm123.aquariumist.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Getter
@Builder
@AllArgsConstructor
public class DtoSession {

    private UUID sessionId;
    private DtoUser user;

}
