package ru.adm123.aquariumist.configuration;

import com.zaxxer.hikari.HikariDataSource;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.sql.DataSource;

@Configuration
@PropertySource("classpath:database.properties")
public class JpaConfiguration {

    @Value("${chat.db.host}")
    private String dbHost;
    @Value("${chat.db.name}")
    private String dbName;
    @Value("${chat.db.port}")
    private String dbPort;
    @Value("${chat.db.user.name}")
    private String dbUserName;
    @Value("${chat.db.user.pass}")
    private String dbUserPass;

    @Bean
    public DataSource dataSource() {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setUsername(dbUserName);
        dataSource.setPassword(dbUserPass);
        dataSource.setJdbcUrl(String.format("jdbc:postgresql://%s:%s/%s", dbHost, dbPort, dbName));
        return dataSource;
    }

    @Bean(initMethod = "migrate")
    public Flyway flyway(DataSource dataSource) {
        Flyway flyway = new Flyway();
        flyway.setDataSource(dataSource);
        return flyway;
    }

}
