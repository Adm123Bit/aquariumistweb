package ru.adm123.aquariumist.configuration;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import ru.adm123.aquariumist.domain.dto.DtoSession;
import ru.adm123.aquariumist.domain.dto.DtoUser;
import ru.adm123.aquariumist.domain.dto.form.DtoFormItemAquariumAdd;
import ru.adm123.aquariumist.domain.entity.EntityItemAquarium;
import ru.adm123.aquariumist.domain.entity.EntitySession;
import ru.adm123.aquariumist.domain.entity.EntityUser;
import ru.adm123.aquariumist.enums.ItemShare;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Configuration
public class MapCofiguration implements WebMvcConfigurer {

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        mapperDtoSession2EntitySession(modelMapper);
        return modelMapper;
    }

    private void mapperDtoSession2EntitySession(ModelMapper modelMapper) {
        modelMapper.addMappings(new PropertyMap<DtoSession, EntitySession>() {
            @Override
            protected void configure() {
            }
        }).setPostConverter(context -> {
            DtoSession source = context.getSource();
            EntitySession destination = context.getDestination();
            destination.setSessionId(source.getSessionId());
            destination.setUserId(source.getUser().getId());
            return destination;
        });
    }

}
