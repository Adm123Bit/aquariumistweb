package ru.adm123.aquariumist.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import ru.adm123.aquariumist.interceptor.InterceptorCacheHeaders;
import ru.adm123.aquariumist.service.ServiceItem;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class MvcConfiguration implements WebMvcConfigurer {

    @Value("${appFile.rootPath}")
    private String filesRootPath;
    @Value("${userFile.userFilesPath}")
    private String filesUserPath;

    @Bean
    public InterceptorCacheHeaders interceptorSession() {
        return new InterceptorCacheHeaders();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(interceptorSession())
                .addPathPatterns("/**");
                //.excludePathPatterns("/pageresources/**");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        getPathMap().forEach((key, value) -> registry.addResourceHandler(key)
                .addResourceLocations(value));
    }

    private Map<String, String> getPathMap() {
        Map<String, String> pathMap = new HashMap<>();
        pathMap.put("/avatar/**", "file:" + filesRootPath + filesUserPath);
        pathMap.put("/defaultImage/**", "file:src" + File.separator + "main" + File.separator + "resources" + File.separator + "static" + File.separator + "pageresources" + File.separator + "images" + File.separator);
        pathMap.put("/itemImage/**", "file:src" + File.separator + "main" + File.separator + "resources" + File.separator + "static" + File.separator + "pageresources" + File.separator + "iconitem" + File.separator);
        pathMap.put("/picture/**", "file:" + filesRootPath + filesUserPath);
        pathMap.put("/cover/**", "file:" + filesRootPath + filesUserPath);
        return pathMap;
    }

}
