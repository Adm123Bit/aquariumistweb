package ru.adm123.aquariumist.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import ru.adm123.aquariumist.service.impl.ServiceFile;

@Component
public class CreatorFilesRootPath implements ApplicationListener<ContextRefreshedEvent> {

    private final ServiceFile serviceFile;

    @Autowired
    public CreatorFilesRootPath(ServiceFile serviceFile) {
        this.serviceFile = serviceFile;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        serviceFile.createAppRootFilePath();
    }

}
