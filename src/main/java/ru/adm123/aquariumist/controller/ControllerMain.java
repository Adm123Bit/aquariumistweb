package ru.adm123.aquariumist.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.adm123.aquariumist.service.impl.ServicePage;

@Controller
public class ControllerMain {

    private final ServicePage servicePage;

    @Autowired
    public ControllerMain(ServicePage servicePage) {
        this.servicePage = servicePage;
    }

    @GetMapping(path = "/")
    public String page(Model model) {
        return servicePage.getPageByName("Main", model);
    }

}
