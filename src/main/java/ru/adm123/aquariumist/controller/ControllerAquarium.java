package ru.adm123.aquariumist.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.adm123.aquariumist.domain.dto.DtoActionResult;
import ru.adm123.aquariumist.domain.dto.DtoItemAquarium;
import ru.adm123.aquariumist.domain.dto.DtoUser;
import ru.adm123.aquariumist.domain.dto.form.*;
import ru.adm123.aquariumist.enums.UserRole;
import ru.adm123.aquariumist.service.impl.ServiceAquarium;
import ru.adm123.aquariumist.service.impl.ServiceFile;
import ru.adm123.aquariumist.service.impl.ServicePage;
import ru.adm123.aquariumist.service.impl.ServiceUser;

import javax.validation.Valid;
import java.io.File;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.UUID;

@Controller
@RequestMapping(path = "/aquarium")
public class ControllerAquarium {

    private final ServicePage servicePage;
    private final ServiceUser serviceUser;
    private final ServiceFile serviceFile;
    private final ServiceAquarium serviceAquarium;

    @Autowired
    public ControllerAquarium(ServicePage servicePage, ServiceUser serviceUser, ServiceFile serviceFile, ServiceAquarium serviceAquarium) {
        this.servicePage = servicePage;
        this.serviceUser = serviceUser;
        this.serviceFile = serviceFile;
        this.serviceAquarium = serviceAquarium;
    }

    @GetMapping(path = "/all")
    public String pageAquariumSelf(Model model) {
        return servicePage.getPageByName("AquariumSelf", model);
    }

    @GetMapping(path = "/all/{contentOwnerLogin}")
    public String pageAquariumUser(@PathVariable final String contentOwnerLogin,
                                   Model model) {
        return servicePage.getPageByName("AquariumUser", serviceUser.getUserByLogin(contentOwnerLogin), model);
    }

    @GetMapping(path = "/{aquariumId}")
    public String pageAquariumItem(@PathVariable final UUID aquariumId,
                                   Model model) {
        //serviceAquarium.clearAddedImages(aquariumId);
        return servicePage.getPageByName("AquariumItem", aquariumId, model);
    }

    @PostMapping(path = "/add")
    public String addAquarium(@Valid @RequestBody DtoFormItemAquariumAdd aquariumData,
                              Model model) {
        serviceAquarium.addItem(aquariumData);
        return servicePage.getPagePartsByName("AquariumSelf", model);
    }

    @PostMapping(path = "/delete")
    public String deleteAquarium(@Valid @RequestBody DtoFormItemAquariumEdit aquariumData,
                                 Model model) {
        serviceAquarium.deleteItem(aquariumData.getId());
        return getPagePart(aquariumData, model);
    }

    @PostMapping(path = "/edit")
    public String editItem(@Valid @RequestBody DtoFormItemAquariumEdit aquariumData,
                           Model model) {
        serviceAquarium.updateItem(aquariumData);
        return getPagePart(aquariumData, model);
    }

    @PostMapping(path = "/unarchive")
    public String unArchiveAquarium(@Valid @RequestBody DtoFormItemAquariumEdit aquariumData,
                                    Model model) {
        serviceAquarium.unArchiveItem(aquariumData.getId());
        return getPagePart(aquariumData, model);
    }

    @PostMapping(path = "/delete/selected")
    public String deleteSelected(@RequestBody DtoFormItemMultiAction multiItemActionData,
                                 Model model) {
        serviceAquarium.deleteItems(multiItemActionData.getItemIds());
        return servicePage.getPagePartsByName(multiItemActionData.getPageTemplate(), model);
    }

    @PostMapping(path = "/archive/selected")
    public String archiveSelected(@RequestBody DtoFormItemMultiAction multiItemActionData,
                                  Model model) {
        serviceAquarium.archiveItems(multiItemActionData.getItemIds());
        return servicePage.getPagePartsByName(multiItemActionData.getPageTemplate(), model);
    }

    @PostMapping(path = "/unarchive/selected")
    public String unArchiveSelected(@RequestBody DtoFormItemMultiAction multiItemActionData,
                                    Model model) {
        serviceAquarium.unarchiveItems(multiItemActionData.getItemIds());
        return servicePage.getPagePartsByName(multiItemActionData.getPageTemplate(), model);
    }

    // todo валидировать форму
    @PostMapping(path = "/add/image")
    @ResponseBody
    public DtoActionResult addImage(@RequestBody DtoFormFile dtoFormFile) {
        DtoActionResult dtoActionResult = new DtoActionResult();
        dtoActionResult.setSuccess(serviceAquarium.addItemImage(dtoFormFile));
        return dtoActionResult;
    }

    // todo валидировать форму
    @PostMapping(path = "/delete/image")
    @ResponseBody
    public DtoActionResult deleteImages(@RequestBody Collection<DtoFormFile> dtoFormFiles) {
        DtoActionResult dtoActionResult = new DtoActionResult();
        dtoActionResult.setSuccess(serviceAquarium.deleteItemImages(dtoFormFiles));
        return dtoActionResult;
    }

    private String getPagePart(DtoFormItemAbstract dtoFormItemAbstract,
                               Model model) {
        if (dtoFormItemAbstract.getPageTemplate().endsWith("Self")) {
            return servicePage.getPagePartsByName(dtoFormItemAbstract.getPageTemplate(), model);
        } else if (dtoFormItemAbstract.getPageTemplate().endsWith("Item")) {
            return servicePage.getPagePartsByName(dtoFormItemAbstract.getPageTemplate(), dtoFormItemAbstract.getId(), model);
        }
        return null;
    }

}
