package ru.adm123.aquariumist.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.adm123.aquariumist.domain.dto.DtoActionResult;
import ru.adm123.aquariumist.domain.dto.DtoLabel;
import ru.adm123.aquariumist.domain.dto.form.*;
import ru.adm123.aquariumist.service.impl.*;

import javax.validation.Valid;

@Controller
@RequestMapping(path = "/label")
public class ControllerActionLabel {

    private final ServiceLabel serviceLabel;

    @Autowired
    public ControllerActionLabel(ServiceLabel serviceLabel) {
        this.serviceLabel = serviceLabel;
    }

    @PostMapping(path = "/add")
    @ResponseBody
    public DtoActionResult addLabel(@Valid @RequestBody DtoFormLabelAdd labelData) {
        DtoActionResult dtoActionResult = new DtoActionResult();
        DtoLabel dtoLabel = serviceLabel.addLabel(labelData);
        dtoActionResult.setSuccess(dtoLabel.getId() != null);
        if (dtoLabel.getId() != null) {
            dtoActionResult.setMessage(dtoLabel.getId().toString());
        }
        return dtoActionResult;
    }

}
