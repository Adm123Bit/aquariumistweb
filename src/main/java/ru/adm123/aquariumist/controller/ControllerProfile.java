package ru.adm123.aquariumist.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.adm123.aquariumist.domain.dto.DtoActionResult;
import ru.adm123.aquariumist.domain.dto.DtoUser;
import ru.adm123.aquariumist.domain.dto.form.*;
import ru.adm123.aquariumist.enums.UserRole;
import ru.adm123.aquariumist.service.impl.ServiceFile;
import ru.adm123.aquariumist.service.impl.ServicePage;
import ru.adm123.aquariumist.service.impl.ServiceSession;
import ru.adm123.aquariumist.service.impl.ServiceUser;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.File;
import java.nio.file.Path;

@Controller
@RequestMapping(path = "/profile")
public class ControllerProfile {

    private final ServicePage servicePage;
    private final ServiceSession serviceSession;
    private final ServiceUser serviceUser;
    private final ServiceFile serviceFile;

    @Autowired
    public ControllerProfile(ServicePage servicePage, ServiceSession serviceSession, ServiceUser serviceUser, ServiceFile serviceFile) {
        this.servicePage = servicePage;
        this.serviceSession = serviceSession;
        this.serviceUser = serviceUser;
        this.serviceFile = serviceFile;
    }

    @RequestMapping(path = "", method = RequestMethod.GET)
    public String page(Model model) {
        return servicePage.getPageByName("Profile", model);
    }

    @PostMapping(path = "/register")
    public String register(@Valid @RequestBody DtoFormUserRegister registerData,
                           HttpServletResponse response,
                           Model model) {
        DtoUser dtoUser = serviceUser.addUser(registerData.getLogin(), registerData.getPassword());
        serviceSession.startSession(dtoUser, response, false);
        return servicePage.getPagePartsByName("Profile", model);
    }

    @PostMapping(path = "/login")
    public String login(@Valid @RequestBody DtoFormUserLogin loginData,
                        HttpServletResponse response,
                        Model model) {
        serviceSession.startSession(serviceUser.getUserByLoginAndPassword(loginData.getLogin(), loginData.getPassword()), response, loginData.isCheck());
        return getPageModel(loginData, model);
    }

    @PostMapping(path = "/logout")
    public String logout(@Valid @RequestBody DtoFormUserLogout logoutData,
                         HttpServletResponse response,
                         Model model) {
        serviceSession.stopSession(response);
        return getPageModel(logoutData, model);
    }

    // todo валидировать форму
    @PostMapping(path = "/edit/avatar")
    @ResponseBody
    public DtoActionResult editAvatar(@RequestBody DtoFormFile fileData) {
        DtoActionResult dtoActionResult = new DtoActionResult();
        dtoActionResult.setSuccess(serviceUser.setUserAvatar(fileData));
        return dtoActionResult;
    }

    @PostMapping(path = "/edit/password")
    @ResponseBody
    public DtoActionResult editPassword(@Valid @RequestBody DtoFormUserEdit userData) {
        serviceUser.setUserPassword(userData.getPassword());
        return new DtoActionResult(true, null);
    }

    private String getPageModel(DtoFormContentUpdateAbstract dtoFormContentUpdateAbstract, Model model) {
        if (dtoFormContentUpdateAbstract.getItemId() != null) {
            return servicePage.getPagePartsByName(dtoFormContentUpdateAbstract.getPageTemplate(), dtoFormContentUpdateAbstract.getItemId(), model);
        } else if (dtoFormContentUpdateAbstract.getContentOwnerLogin() != null) {
            return servicePage.getPagePartsByName(dtoFormContentUpdateAbstract.getPageTemplate(), serviceUser.getUserByLogin(dtoFormContentUpdateAbstract.getContentOwnerLogin()), model);
        }
        return servicePage.getPagePartsByName(dtoFormContentUpdateAbstract.getPageTemplate(), model);
    }

}
