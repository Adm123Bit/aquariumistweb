package ru.adm123.aquariumist.interceptor;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import ru.adm123.aquariumist.domain.dto.DtoSession;
import ru.adm123.aquariumist.domain.dto.DtoUser;
import ru.adm123.aquariumist.service.impl.ServiceSession;
import ru.adm123.aquariumist.service.impl.ServiceUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@NoArgsConstructor
public class InterceptorCacheHeaders implements HandlerInterceptor {

    @Autowired
    private ServiceUser serviceUser;
    @Autowired
    private ServiceSession serviceSession;

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response,
                             Object handler) {
        DtoUser dtoUser = serviceUser.getCurrentUser();
        if (dtoUser == null) {
            UUID userCookieSessionId = serviceSession.getCookieSessionId(request);
            if (userCookieSessionId != null) {
                DtoSession dtoSession = serviceSession.getSavedSession(userCookieSessionId);
                if (dtoSession != null) {
                    serviceSession.refreshSession(dtoSession, response, true);
                } else {
                    serviceSession.startGuestSession();
                }
            } else {
                serviceSession.startGuestSession();
            }
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response,
                           Object handler,
                           @Nullable ModelAndView modelAndView) {
        setNoCacheHeaders(response);
        DtoUser dtoUser = serviceUser.getCurrentUser();
        if (dtoUser == null) {
            serviceSession.stopSession(response);
        }
    }

    private void setNoCacheHeaders(HttpServletResponse response) {
        response.addHeader("Expires", "0");
        response.addHeader("Cache-Control", "no-store");
        response.addHeader("Cache-Control", "no-cache, no-store , must-revalidate");
    }

}
