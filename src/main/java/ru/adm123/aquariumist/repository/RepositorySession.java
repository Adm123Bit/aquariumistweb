package ru.adm123.aquariumist.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.adm123.aquariumist.domain.entity.EntitySession;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface RepositorySession extends CrudRepository<EntitySession, UUID> {

    /*Optional<EntitySession> findBySessionId(UUID sessionId);

/*    @Query(value = "delete from user_session where session_id = :sessionId", nativeQuery = true)
    void deleteBySessionId(@Param("sessionId") UUID sessionId);

    @Query(value = "delete from user_session where user_id = :userId", nativeQuery = true)
    void deleteByUserId(@Param("userId") long userId);*/

}
