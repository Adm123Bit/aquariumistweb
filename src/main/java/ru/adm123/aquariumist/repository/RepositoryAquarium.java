package ru.adm123.aquariumist.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.adm123.aquariumist.domain.entity.EntityItemAquarium;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface RepositoryAquarium extends CrudRepository<EntityItemAquarium, UUID> {

    Iterable<EntityItemAquarium> findAllByOwner_IdAndDeletedFalseOrderByAddDateTimeDesc(long ownerId);

    Optional<EntityItemAquarium> findAllByIdAndDeletedFalse(UUID aquariumId);

}

