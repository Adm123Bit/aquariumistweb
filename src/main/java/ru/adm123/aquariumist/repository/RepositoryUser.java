package ru.adm123.aquariumist.repository;

import org.springframework.data.repository.CrudRepository;
import ru.adm123.aquariumist.domain.entity.EntityUser;

import java.util.Optional;

public interface RepositoryUser extends CrudRepository<EntityUser, Long> {

    Optional<EntityUser> findByLogin(String login);

    Optional<EntityUser> findByLoginAndPassword(String login, String password);

}
