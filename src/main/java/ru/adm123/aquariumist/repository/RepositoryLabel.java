package ru.adm123.aquariumist.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.adm123.aquariumist.domain.entity.EntityLabel;
import ru.adm123.aquariumist.enums.ItemType;

import java.util.UUID;

@Repository
public interface RepositoryLabel extends CrudRepository<EntityLabel, UUID> {

    Iterable<EntityLabel> findByOwner_IdAndTypeAndDeletedFalseOrderByNameAsc(long ownerId, ItemType itemType);

}