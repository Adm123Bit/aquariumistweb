package ru.adm123.aquariumist.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.adm123.aquariumist.domain.entity.EntityItemAbstract;
import ru.adm123.aquariumist.domain.entity.EntityItemAquarium;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface RepositoryItem extends CrudRepository<EntityItemAbstract, UUID> {

}

