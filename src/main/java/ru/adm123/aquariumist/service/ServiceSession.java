package ru.adm123.aquariumist.service;

import ru.adm123.aquariumist.domain.dto.DtoSession;
import ru.adm123.aquariumist.domain.dto.DtoUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

public interface ServiceSession {

    void startSession(DtoUser dtoUser, HttpServletResponse response, boolean rememberUser);

    void stopSession(HttpServletResponse response);

    void refreshSession(DtoSession dtoSession, HttpServletResponse response, boolean rememberUser);

    void startGuestSession();

    /*DtoSession getCurrentSession();*/

    DtoSession getSavedSession(UUID sessionId);

    UUID getCookieSessionId(HttpServletRequest request);

/*    String getSessionIdFromCookie(HttpServletRequest request);

    DtoSession getSessionById(UUID sessionId);*/

}
