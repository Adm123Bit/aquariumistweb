package ru.adm123.aquariumist.service;

import ru.adm123.aquariumist.domain.dto.DtoSession;
import ru.adm123.aquariumist.domain.dto.DtoUser;
import ru.adm123.aquariumist.domain.dto.form.DtoFormFile;

import java.util.Collection;
import java.util.UUID;

public interface ServiceUser {

/*    DtoSession getSession();*/

/*    DtoUser getSession(UUID sessionId);*/

    DtoUser getUserById(long userId);

    DtoUser getUserByLogin(String userLogin);

    DtoUser getUserByLoginAndPassword(String login, String password);

    boolean isUserExists(String login);

    DtoUser getCurrentUser();

    DtoUser addUser(String login, String password);

    //void setUserAvatar(/*DtoUser dtoUser*/);

    void setUserPassword(String newPassword);

    DtoUser getContentOwnerById(Long userId);

    boolean isUserOwnerOfItem(String userLogin, UUID itemId);

    boolean setUserAvatar(DtoFormFile dtoFormFile);

    String getUserAvatar(DtoUser dtoUser);

}
