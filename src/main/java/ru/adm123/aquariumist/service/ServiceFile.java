package ru.adm123.aquariumist.service;

import lombok.SneakyThrows;
import ru.adm123.aquariumist.annotation.Overload;
import ru.adm123.aquariumist.domain.dto.DtoItemAbstract;
import ru.adm123.aquariumist.domain.dto.DtoUser;
import ru.adm123.aquariumist.domain.dto.form.DtoFormFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ServiceFile {

    default Path mkDir(Path path) {
        try {
            return Files.createDirectories(path);
        } catch (Exception e) {
            return null;
        }
    }

    @SneakyThrows
    default void clearDir(Path path) {
        if (Files.exists(path)) {
            Files.walk(path, 1).forEach(fPath -> {
                try {
                    Files.delete(fPath);
                } catch (Exception ignore) {
                }
            });
        }
    }

    void createAppRootFilePath();

    void createUserFilePath(DtoUser dtoUser);

    void createItemFilePath(DtoItemAbstract dtoItemAbstract);

    Path getUserFilePath(String userLogin);

    @Overload
    Path getItemFilePath(String userLogin, UUID itemId);

    @Overload
    Path getItemFilePath(DtoItemAbstract dtoItemAbstract);

    @Overload
    Path getItemCoverPath(DtoItemAbstract dtoItemAbstract);

    @Overload
    Path getItemAddedFilePath(DtoItemAbstract dtoItemAbstract);

    @Overload
    Path getItemDeletedFilePath(DtoItemAbstract dtoItemAbstract);

    /*    @Overload
    void deleteFile(File file);*/

    //@Overload
    void deleteFile(Path file);

    boolean deleteFiles(Path filePath, /*Collection<String> fileNames*/Collection<DtoFormFile> dtoFormFiles);

    boolean deleteFilesWithExclude(Path filePath, String... excludeFiles);

    //File saveBase64File(Path filePath, DtoFormFile fileData);

    File saveBase64File(Path filePath, String fileName, String base64Body);

    Collection<File> findAllInDirectory(Path filePath);

    Optional<File> findByName(Path filePath, String fileName);

    //String getFileMime(File file);

}
