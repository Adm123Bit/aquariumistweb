package ru.adm123.aquariumist.service;

import org.springframework.ui.Model;
import ru.adm123.aquariumist.annotation.Overload;
import ru.adm123.aquariumist.domain.dto.DtoUser;

import java.util.Collection;
import java.util.UUID;

public interface ServicePage {

    @Overload
    String getPageByName(String pageName, Model model);

    @Overload
    String getPagePartsByName(String pageName, Model model);

    @Overload
    String getPageByName(String pageName, DtoUser contentOwner, Model model);

    @Overload
    String getPagePartsByName(String pageName, DtoUser contentOwner, Model model);

    @Overload
    String getPageByName(String pageName, UUID itemId, Model model);

    @Overload
    String getPagePartsByName(String pageName, UUID itemId, Model model);

}
