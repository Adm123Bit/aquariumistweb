package ru.adm123.aquariumist.service.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.adm123.aquariumist.domain.dto.DtoSession;
import ru.adm123.aquariumist.domain.dto.DtoUser;
import ru.adm123.aquariumist.domain.entity.EntitySession;
import ru.adm123.aquariumist.enums.UserRole;
import ru.adm123.aquariumist.repository.RepositorySession;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.UUID;

@Service
public class ServiceSession implements ru.adm123.aquariumist.service.ServiceSession {

    private final RepositorySession repositorySession;
    private final ServiceUser serviceUser;
    private final ModelMapper modelMapper;
    private static final String COOKIE_NAME = "AquariumistSession";
    private static final int COOKIE_EXPIRE = 3600 * 24 * 30 * 12; // about 1 year
    private final HttpSession httpSession;

    @Autowired
    public ServiceSession(RepositorySession repositorySession, ServiceUser serviceUser, ModelMapper modelMapper, HttpSession httpSession) {
        this.repositorySession = repositorySession;
        this.serviceUser = serviceUser;
        this.modelMapper = modelMapper;
        this.httpSession = httpSession;
    }

    @Override
    @Transactional
    public void startSession(DtoUser dtoUser, HttpServletResponse response, boolean rememberUser) {
        UUID sessionId = UUID.randomUUID();
        DtoSession dtoSession = new DtoSession(sessionId, dtoUser);
        httpSession.setAttribute("userSession"/*httpSession.getId()*/, dtoSession);
        if (rememberUser) {
            setCookie(response, sessionId);
            repositorySession.save(modelMapper.map(dtoSession, EntitySession.class));/*new EntitySession(dtoUser.getUserId(), sessionId));*/
        }
        dtoUser.setAvatar(serviceUser.getUserAvatar(dtoUser));
    }

    @Override
    @Transactional
    public void stopSession(HttpServletResponse response) {
        DtoSession dtoSession = (DtoSession) httpSession.getAttribute(/*httpSession.getId()*/"userSession");
        clearCookie(response);
        startGuestSession();
        if (dtoSession != null) {
            //httpSession.removeAttribute(/*httpSession.getId()*/"userSession");
            repositorySession.delete(modelMapper.map(dtoSession, EntitySession.class));
        }
    }

    @Override
    public void refreshSession(DtoSession dtoSession, HttpServletResponse response, boolean rememberUser) {
        httpSession.setAttribute("userSession"/*httpSession.getId()*/, dtoSession);
        if (rememberUser) {
            setCookie(response, dtoSession.getSessionId());
        }
        DtoUser currentUser = dtoSession.getUser();
        currentUser.setAvatar(serviceUser.getUserAvatar(currentUser));
        //serviceUser.setUserAvatar(/*dtoSession.getUser()*/);
    }

    @Override
    public void startGuestSession() {
        DtoUser dtoEmptyUser = new DtoUser();
        dtoEmptyUser.setRole(UserRole.GUEST);
        dtoEmptyUser.setId(0L);
        DtoSession dtoSession = new DtoSession(UUID.fromString("00000000-0000-0000-0000-00000000000"), dtoEmptyUser);
        httpSession.setAttribute("userSession"/*httpSession.getId()*/, dtoSession);
    }

    @Override
    @Transactional
    public DtoSession getSavedSession(UUID sessionId) {
        return repositorySession.findById(sessionId)
                .map(entitySession -> {
                    DtoUser dtoUser = serviceUser.getUserById(entitySession.getUserId());
                    return dtoUser == null ? null : new DtoSession(sessionId, dtoUser);
                }).orElse(null);
    }

    @Override
    public UUID getCookieSessionId(HttpServletRequest request) {
        if (request.getCookies() == null || request.getCookies().length == 0) {
            return null;
        }
        return Arrays.stream(request.getCookies())
                .filter(cookie -> cookie.getName().equals(COOKIE_NAME))
                .findFirst()
                .map(cookie -> UUID.fromString(cookie.getValue()))
                .orElse(null);
    }

    private void setCookie(HttpServletResponse response, UUID sessionId) {
        Cookie cookie = new Cookie(COOKIE_NAME, sessionId.toString());
        cookie.setMaxAge(COOKIE_EXPIRE);
        cookie.setPath("/");
        response.addCookie(cookie);
    }

    private void clearCookie(HttpServletResponse response) {
        Cookie cookie = new Cookie(COOKIE_NAME, "");
        cookie.setMaxAge(0);
        cookie.setPath("/");
        response.addCookie(cookie);
    }

}
