package ru.adm123.aquariumist.service.impl;

import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.adm123.aquariumist.annotation.Overload;
import ru.adm123.aquariumist.domain.dto.DtoItemAbstract;
import ru.adm123.aquariumist.domain.dto.DtoUser;
import ru.adm123.aquariumist.domain.dto.form.DtoFormFile;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ServiceFile implements ru.adm123.aquariumist.service.ServiceFile {

    @Value("${appFile.rootPath}")
    private String filesRootPath;
    @Value("${userFile.userFilesPath}")
    private String filesUsersPath;

    @SneakyThrows
    @Override
    public void createAppRootFilePath() {
        Files.createDirectories(Paths.get(filesRootPath + filesUsersPath));
    }

    @SneakyThrows
    @Override
    public void createUserFilePath(DtoUser dtoUser) {
        Files.createDirectories(Paths.get(filesRootPath + filesUsersPath + dtoUser.getLogin()));
    }

    @SneakyThrows
    @Override
    public void createItemFilePath(DtoItemAbstract dtoItemAbstract) {
        //Path itemRoot =
                Files.createDirectories(Paths.get(filesRootPath + filesUsersPath + dtoItemAbstract.getOwner().getLogin() + File.separator + dtoItemAbstract.getId()));
/*        Files.createDirectory(Paths.get(itemRoot + File.separator + "deleted"));
        Files.createDirectory(Paths.get(itemRoot + File.separator + "added"));
        Files.createDirectory(Paths.get(itemRoot + File.separator + "cover"));*/
    }

    //@SneakyThrows
    @Override
    public Path getUserFilePath(String userLogin) {
        /*Path userPath = Paths.get(filesRootPath + filesUsersPath + userLogin);
        Files.createDirectories(userPath);
        return userPath;*/
        return Paths.get(filesRootPath + filesUsersPath + userLogin);
    }

    //@SneakyThrows
    @Override
    @Overload
    public Path getItemFilePath(String userLogin, UUID itemId) {
        /*Path itemPath = Paths.get(filesRootPath + filesUsersPath + userLogin + File.separator + itemId);
        Files.createDirectories(itemPath);
        return itemPath;*/
        return Paths.get(filesRootPath + filesUsersPath + userLogin + File.separator + itemId);
    }

    //@SneakyThrows
    @Override
    @Overload
    public Path getItemFilePath(DtoItemAbstract dtoItemAbstract) {
        /*Path itemPath = Paths.get(filesRootPath + filesUsersPath + dtoItemAbstract.getOwner().getLogin() + File.separator + dtoItemAbstract.getId());
        Files.createDirectories(itemPath);
        return itemPath;*/
        return Paths.get(filesRootPath + filesUsersPath + dtoItemAbstract.getOwner().getLogin() + File.separator + dtoItemAbstract.getId());
    }

    //@SneakyThrows
    @Override
    public Path getItemCoverPath(DtoItemAbstract dtoItemAbstract) {
        /*Path itemCoverPath = Paths.get(getItemFilePath(dtoItemAbstract) + File.separator + "cover" + File.separator);
        Files.createDirectories(itemCoverPath);
        return itemCoverPath;*/
        return Paths.get(getItemFilePath(dtoItemAbstract) + File.separator + "cover" + File.separator);
    }

    @Override
    public Path getItemAddedFilePath(DtoItemAbstract dtoItemAbstract) {
        return Paths.get(getItemFilePath(dtoItemAbstract) + File.separator + "added" + File.separator);
    }

    @Override
    public Path getItemDeletedFilePath(DtoItemAbstract dtoItemAbstract) {
        return Paths.get(getItemFilePath(dtoItemAbstract) + File.separator + "deleted" + File.separator);
    }
/*
    @Override
    @Overload
    public void deleteFile(File file) {
        file.delete();
    }*/

    @SneakyThrows
    @Override
    //@Overload
    public void deleteFile(Path file) {
        Path fName = file.getName(file.getNameCount() - 1);
        String deletedFilePath = file.toString().replace(fName.toString(), "deleted" + File.separator);
        Files.createDirectories(Paths.get(deletedFilePath));
        Files.move(file, Paths.get(deletedFilePath + fName));
    }

    //@SneakyThrows
    @Override
    public boolean deleteFiles(Path filePath, /*Collection<String> exceptFileNames*/Collection<DtoFormFile> dtoFormFiles) {
        try {
            Files.walk(filePath, 1)
                    .filter(path -> {
                        if (Files.isRegularFile(path)) {
                            return dtoFormFiles.stream()
                                    .anyMatch(dtoFormFile -> path.endsWith(dtoFormFile.getFileName()));
                        }
                        return false;
                    })
                    .forEach(this::deleteFile);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    //@SneakyThrows
    @Override
    public boolean deleteFilesWithExclude(Path filePath, String... excludeFileNames) {
        try {
            Files.walk(filePath, 1)
                    .filter(path -> {
                        if (Files.isRegularFile(path)) {
                            if (excludeFileNames.length > 0) {
                                for (String filename : excludeFileNames) {
                                    if (filename != null && path.endsWith(filename)) {
                                        return false;
                                    }
                                }
                            }
                            return true;
                        }
                        return false;
                    })
                    .forEach(this::deleteFile);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public File saveBase64File(Path filePath, String fileName, String base64Body) {
        byte[] bytes = Base64.getDecoder().decode(base64Body);
        File newFile = new File(filePath + File.separator + fileName);
        return byteArrToFile(newFile, bytes);
    }

    //@SneakyThrows
    @Override
    public Collection<File> findAllInDirectory(Path filePath) {
        try {
            return Files.walk(filePath, 1)
                    .filter(Files::isRegularFile)
                    .map(Path::toFile)
                    //.sorted()
                    .collect(Collectors.toList());
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    @Override
    public Optional<File> findByName(Path filePath, String fileName) {
        return findAllInDirectory(filePath).stream()
                .filter(file -> file.getName().matches(fileName + ".[a-zA-Z]{3,4}\\b"))
                .findFirst();
    }

/*    @Override
    public String getFileMime(File file) {
        try {
            return Files.probeContentType(file.toPath());
        } catch (IOException e) {
            return null;
        }
    }*/

    private File byteArrToFile(File newFile, byte[] bytes) {
        try {
            FileUtils.writeByteArrayToFile(newFile, bytes);
            return newFile;
        } catch (Exception e) {
            return null;
        }
    }

}
