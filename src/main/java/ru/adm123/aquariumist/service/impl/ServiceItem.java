package ru.adm123.aquariumist.service.impl;

import lombok.SneakyThrows;
import org.springframework.transaction.annotation.Transactional;
import ru.adm123.aquariumist.aspect.annotation.IfCurrentUserIsOwner;
import ru.adm123.aquariumist.domain.dto.DtoItemAbstract;
import ru.adm123.aquariumist.domain.dto.DtoLabel;
import ru.adm123.aquariumist.domain.dto.DtoUser;
import ru.adm123.aquariumist.domain.dto.form.DtoFormFile;
import ru.adm123.aquariumist.domain.dto.form.DtoFormItemAbstract;
import ru.adm123.aquariumist.domain.entity.EntityItemAbstract;
import ru.adm123.aquariumist.domain.entity.EntityLabel;
import ru.adm123.aquariumist.repository.RepositoryItem;
import ru.adm123.aquariumist.repository.RepositoryLabel;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public abstract class ServiceItem<T extends DtoItemAbstract, FA extends DtoFormItemAbstract, FE extends DtoFormItemAbstract> implements ru.adm123.aquariumist.service.ServiceItem<T, FA, FE> {

    private final RepositoryItem repositoryItem;
    //private final RepositoryLabel repositoryLabel;
    private final ServiceFile serviceFile;
    private final ServiceUser serviceUser;
    //private final ServiceLabel serviceLabel;
    //private final ModelMapper modelMapper;

    public ServiceItem(/*ModelMapper modelMapper, */RepositoryItem repositoryItem, /*RepositoryLabel repositoryLabel, */ServiceFile serviceFile, ServiceUser serviceUser) {
        this.repositoryItem = repositoryItem;
        //this.repositoryLabel = repositoryLabel;
        this.serviceFile = serviceFile;
        this.serviceUser = serviceUser;
        //this.modelMapper = modelMapper;
        //this.serviceLabel = serviceLabel;
    }

    @Override
    @IfCurrentUserIsOwner
    @Transactional
    public boolean deleteItem(UUID itemId) {
        EntityItemAbstract entityItemAbstract = repositoryItem.findById(itemId).orElse(null);
        if (entityItemAbstract == null) {
            return false;
        }
        entityItemAbstract.setDeleted(true);
        entityItemAbstract.setEditDateTime(LocalDateTime.now());
        repositoryItem.save(entityItemAbstract);
        return true;
    }

    @Override
    @IfCurrentUserIsOwner
    @Transactional
    public boolean unArchiveItem(UUID itemId) {
        EntityItemAbstract entityItemAbstract = repositoryItem.findById(itemId).orElse(null);
        if (entityItemAbstract == null) {
            return false;
        }
        entityItemAbstract.setArchived(false);
        entityItemAbstract.setEditDateTime(LocalDateTime.now());//Считаем, что восстановление из архива - тоже редактирование
        repositoryItem.save(entityItemAbstract);
        return true;
    }

    @Override
    @IfCurrentUserIsOwner
    @Transactional
    public boolean deleteItems(Collection<UUID> itemIds) {
        LocalDateTime now = LocalDateTime.now();
        Iterable<EntityItemAbstract> itemsToDelete = repositoryItem.findAllById(itemIds);
        itemsToDelete.forEach(entityItem -> {
            entityItem.setDeleted(true);
            entityItem.setEditDateTime(now);
        });
        repositoryItem.saveAll(itemsToDelete);
        return true;
    }

    @Override
    @IfCurrentUserIsOwner
    @Transactional
    public boolean archiveItems(Collection<UUID> itemIds) {
        LocalDateTime now = LocalDateTime.now();
        Iterable<EntityItemAbstract> itemsToArchive = repositoryItem.findAllById(itemIds);
        itemsToArchive.forEach(entityItem -> {
            if (!entityItem.isArchived()) {
                entityItem.setArchived(true);
                entityItem.setEditDateTime(now);
            }
        });
        repositoryItem.saveAll(itemsToArchive);
        return true;
    }

    @Override
    @IfCurrentUserIsOwner
    @Transactional
    public boolean unarchiveItems(Collection<UUID> itemIds) {
        LocalDateTime now = LocalDateTime.now();
        Iterable<EntityItemAbstract> itemsToUnarchive = repositoryItem.findAllById(itemIds);
        itemsToUnarchive.forEach(entityItem -> {
            if (entityItem.isArchived()) {
                entityItem.setArchived(false);
                entityItem.setEditDateTime(now);
            }
        });
        repositoryItem.saveAll(itemsToUnarchive);
        return true;
    }

    @Override
    public Collection<DtoFormFile> getImagesForItem(DtoItemAbstract dtoItemAbstract) {
        return serviceFile.findAllInDirectory(serviceFile.getItemFilePath(dtoItemAbstract)).stream()
                .map(file -> {
                    DtoFormFile dtoFormFile = new DtoFormFile();
                    //dtoFormFile.setFileType(FileType.IMAGE);
                    dtoFormFile.setFileName(file.getName());
                    //dtoFormFile.setFileMime(serviceFile.getFileMime(file));
                    dtoFormFile.setItemId(dtoItemAbstract.getId());
                    dtoFormFile.setOwnerLogin(dtoItemAbstract.getOwner().getLogin());
                    //".." + File.separator + ".." + File.separator + "picture" + File.separator + dtoItemAbstract.getOwner().getId() + File.separator + dtoItemAbstract.getId() + File.separator + file.getName()
                    return dtoFormFile;
                })
                .collect(Collectors.toList());
    }

/*    @Override
    public Collection<DtoLabel> getLabelsForItem(EntityItemAbstract entityItemAbstract) {
        //Collection<DtoLabel> result = new ArrayList<>();
        List<UUID> labelList = entityItemAbstract.getLabels().stream()
                .map(EntityLabel::getId)
                .collect(Collectors.toList());
        return serviceLabel.getAllById(labelList);
    }  */

    //@SneakyThrows
    @Override
    public String getItemCover(DtoItemAbstract dtoItemAbstract) {
        try {
            //Path coverPath = serviceFile.getItemCoverPath(dtoItemAbstract);
            return Files.walk(serviceFile.getItemCoverPath(dtoItemAbstract), 1)
                    .filter(filePath -> Files.isRegularFile(filePath))
                    .findFirst()
                    .map(filePath -> filePath.getFileName().toString())
                    .orElse(null);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public boolean addItemImage(DtoFormFile dtoFormFile) {
        DtoUser currentUser = serviceUser.getCurrentUser();
        if (dtoFormFile.getOwnerLogin().equals(currentUser.getLogin())) {
            Path targetFilePath = Paths.get(serviceFile.getItemFilePath(currentUser.getLogin() + File.separator, dtoFormFile.getItemId()) + File.separator + "added");
            if (!Files.exists(targetFilePath) && serviceFile.mkDir(targetFilePath) == null) {
                return false;
            }
            serviceFile.saveBase64File(targetFilePath, dtoFormFile.getFileName(), dtoFormFile.getFileBase64Body());
            //return dtoFormFile.getItemId();
            return setEditDateAsNow(dtoFormFile.getItemId());
        }
        return false;
    }

    @Override
    public boolean deleteItemImages(Collection<DtoFormFile> dtoFormFiles) {
        DtoUser currentUser = serviceUser.getCurrentUser();
        Collection<DtoFormFile> currentUserFiles = dtoFormFiles.stream()
                .filter(dtoFormFile -> dtoFormFile.getOwnerLogin().equals(currentUser.getLogin()))
                .collect(Collectors.toList());
        if (currentUserFiles.size() != dtoFormFiles.size()) {
            return false;
        }
        if (!currentUserFiles.isEmpty()) {
            UUID itemId = currentUserFiles.stream()
                    .findFirst()
                    .map(DtoFormFile::getItemId)
                    .orElse(null);
            if (itemId != null) {
                Path itemFilePath = serviceFile.getItemFilePath(currentUser.getLogin(), itemId);
                if (serviceFile.deleteFiles(itemFilePath, currentUserFiles)) {
                    return setEditDateAsNow(itemId);
                } else {
                    return false;
                }
            }
            return false;
        }
        return false;
    }

    @SneakyThrows
    @Override
    public void clearAddedImages(/*UUID itemId*/DtoItemAbstract dtoItemAbstract) {
        //DtoUser currentUser = serviceUser.getCurrentUser();
        Path addedImagesPath = Paths.get(serviceFile.getItemFilePath(dtoItemAbstract) + "added");
        Files.deleteIfExists(addedImagesPath);
    }

    private boolean setEditDateAsNow(UUID itemId) {
        if (itemId != null) {
            EntityItemAbstract entityItemAbstract = repositoryItem.findById(itemId).orElse(null);
            if (entityItemAbstract != null) {
                entityItemAbstract.setEditDateTime(LocalDateTime.now());
                repositoryItem.save(entityItemAbstract);
                return true;
            }
            return false;
        }
        return false;
    }

    // Todo сделать проверку через аспект
    @Transactional
    @Override
    public EntityItemAbstract getEntityForCurrentUser(UUID entityId) {
        EntityItemAbstract entityItemAbstract = repositoryItem.findById(entityId).orElse(null);
        if (entityItemAbstract != null) {
            if (entityItemAbstract.getOwner().getId().longValue() != serviceUser.getCurrentUser().getId()) {
                return null;
            }
        }
        return entityItemAbstract;
    }

}
