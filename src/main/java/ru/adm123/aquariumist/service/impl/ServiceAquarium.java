package ru.adm123.aquariumist.service.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.adm123.aquariumist.aspect.annotation.FilterByShare;
import ru.adm123.aquariumist.domain.dto.DtoItemAquarium;
import ru.adm123.aquariumist.domain.dto.form.DtoFormItemAquariumAdd;
import ru.adm123.aquariumist.domain.dto.form.DtoFormItemAquariumEdit;
import ru.adm123.aquariumist.domain.entity.EntityItemAquarium;
import ru.adm123.aquariumist.domain.entity.EntityLabel;
import ru.adm123.aquariumist.domain.entity.EntityUser;
import ru.adm123.aquariumist.repository.RepositoryAquarium;
import ru.adm123.aquariumist.repository.RepositoryItem;
import ru.adm123.aquariumist.repository.RepositoryLabel;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ServiceAquarium extends ServiceItem<DtoItemAquarium, DtoFormItemAquariumAdd, DtoFormItemAquariumEdit> {

    private final RepositoryAquarium repositoryAquarium;
    private final RepositoryLabel repositoryLabel;
    private final ModelMapper modelMapper;
    private final ServiceUser serviceUser;
    private final ServiceFile serviceFile;
    //private final RepositoryItem repositoryItem;

    @Autowired
    public ServiceAquarium(RepositoryAquarium repositoryAquarium, RepositoryLabel repositoryLabel, ModelMapper modelMapper, ServiceUser serviceUser, RepositoryItem repositoryItem, ServiceFile serviceFile) {
        super(/*modelMapper, */repositoryItem, /*repositoryLabel, */serviceFile, serviceUser);
        this.repositoryAquarium = repositoryAquarium;
        this.repositoryLabel = repositoryLabel;
        this.modelMapper = modelMapper;
        this.serviceUser = serviceUser;
        this.serviceFile = serviceFile;
    }

    @Override
    @Transactional
    @FilterByShare
    public Collection<DtoItemAquarium> getAllForUser(long userId, boolean showPrivate) {
        Iterable<EntityItemAquarium> userAquariums = repositoryAquarium.findAllByOwner_IdAndDeletedFalseOrderByAddDateTimeDesc(userId);
        return StreamSupport.stream(userAquariums.spliterator(), false)
                .map(aquariumEntity -> {
                    DtoItemAquarium dtoItemAquarium = modelMapper.map(aquariumEntity, DtoItemAquarium.class);
                    dtoItemAquarium.setCover(getItemCover(dtoItemAquarium));
                    return dtoItemAquarium;
                })
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public DtoItemAquarium addItem(DtoFormItemAquariumAdd dtoFormItemAquariumAdd) {
        EntityItemAquarium entityItemAquarium = modelMapper.map(dtoFormItemAquariumAdd, EntityItemAquarium.class);
        entityItemAquarium.setOwner(modelMapper.map(serviceUser.getCurrentUser(), EntityUser.class));
        entityItemAquarium.setAddDateTime(LocalDateTime.now());
        entityItemAquarium.setEditDateTime(entityItemAquarium.getAddDateTime());
        entityItemAquarium = repositoryAquarium.save(entityItemAquarium);
        DtoItemAquarium dtoItemAquarium = modelMapper.map(entityItemAquarium, DtoItemAquarium.class);
        serviceFile.createItemFilePath(dtoItemAquarium);
        return dtoItemAquarium;
    }

    @Override
    @Transactional
    @FilterByShare
    public DtoItemAquarium getItem(UUID aquariumId) {
        return repositoryAquarium.findAllByIdAndDeletedFalse(aquariumId)
                .map(entityAquarium -> {
                    DtoItemAquarium dtoItemAquarium = modelMapper.map(entityAquarium, DtoItemAquarium.class);
                    serviceFile.clearDir(serviceFile.getItemAddedFilePath(dtoItemAquarium));
                    return dtoItemAquarium;
                })
                .orElse(null);
    }

    @Override
    @Transactional
    public void updateItem(DtoFormItemAquariumEdit aquariumData) {
        EntityItemAquarium entityItemAquarium = (EntityItemAquarium) getEntityForCurrentUser(aquariumData.getId());
        if (entityItemAquarium == null) {
            return;
        }
        List<EntityLabel> labelList = new ArrayList<>();
        repositoryLabel.findAllById(aquariumData.getLabelIds()).forEach(labelList::add);
        entityItemAquarium.setLabels(labelList);
        entityItemAquarium.setName(aquariumData.getName());
        entityItemAquarium.setDescription(aquariumData.getDescription());
        entityItemAquarium.setShare(aquariumData.getShare());
        entityItemAquarium.setArchived(aquariumData.isArchived());
        entityItemAquarium.setVolume(aquariumData.getVolume());
        entityItemAquarium.setStartDate(aquariumData.getStartDate());
        entityItemAquarium.setEditDateTime(LocalDateTime.now());
        repositoryAquarium.save(entityItemAquarium);
    }

}
