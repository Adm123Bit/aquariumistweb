package ru.adm123.aquariumist.service.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.adm123.aquariumist.domain.dto.DtoLabel;
import ru.adm123.aquariumist.domain.dto.form.DtoFormLabelAdd;
import ru.adm123.aquariumist.domain.entity.EntityLabel;
import ru.adm123.aquariumist.domain.entity.EntityUser;
import ru.adm123.aquariumist.enums.ItemType;
import ru.adm123.aquariumist.repository.RepositoryLabel;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.TreeSet;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ServiceLabel implements ru.adm123.aquariumist.service.ServiceLabel {

    private final RepositoryLabel repositoryLabel;
    private final ServiceUser serviceUser;
    private final ModelMapper modelMapper;

    @Autowired
    public ServiceLabel(ModelMapper modelMapper, RepositoryLabel repositoryLabel, ServiceUser serviceUser) {
        this.modelMapper = modelMapper;
        this.repositoryLabel = repositoryLabel;
        this.serviceUser = serviceUser;
    }

    @Override
    @Transactional
    public Collection<DtoLabel> getAllForCurrentUserByItemType(ItemType itemType) {
        Iterable<EntityLabel> entityLabels = repositoryLabel.findByOwner_IdAndTypeAndDeletedFalseOrderByNameAsc(serviceUser.getCurrentUser().getId(), itemType);
        return StreamSupport.stream(entityLabels.spliterator(), false)
                .map(entityLabel -> modelMapper.map(entityLabel, DtoLabel.class))
                .filter(dtoItemLabel -> !dtoItemLabel.isDeleted())
                .collect(Collectors.toList());
    }

/*    @Override
    public Collection<DtoLabel> getAllById(Collection<UUID> labelIds) {
        Collection<DtoLabel> result = new TreeSet<>();
        repositoryLabel.findAllById(labelIds).forEach(entityLabel -> {
            result.add(modelMapper.map(entityLabel, DtoLabel.class));
        });
        return result;
    }*/

    @Override
    @Transactional
    public DtoLabel addLabel(DtoFormLabelAdd dtoFormLabelAdd) {
        EntityLabel entityLabel = new EntityLabel();
        entityLabel.setName(dtoFormLabelAdd.getName());
        entityLabel.setColor(dtoFormLabelAdd.getColor());
        entityLabel.setType(dtoFormLabelAdd.getType());
        entityLabel.setOwner(modelMapper.map(serviceUser.getCurrentUser(), EntityUser.class));
        LocalDateTime now = LocalDateTime.now();
        entityLabel.setAddDateTime(now);
        entityLabel.setEditDateTime(now);
        return modelMapper.map(repositoryLabel.save(entityLabel), DtoLabel.class);
    }

}