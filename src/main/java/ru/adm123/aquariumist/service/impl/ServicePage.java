package ru.adm123.aquariumist.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import ru.adm123.aquariumist.annotation.Overload;
import ru.adm123.aquariumist.domain.dto.DtoItemAbstract;
import ru.adm123.aquariumist.domain.dto.DtoItemTypeCard;
import ru.adm123.aquariumist.domain.dto.DtoUser;
import ru.adm123.aquariumist.enums.ItemType;
import ru.adm123.aquariumist.enums.UserRole;
import ru.adm123.aquariumist.service.ServiceItem;

import java.util.*;

@Service
public class ServicePage implements ru.adm123.aquariumist.service.ServicePage {

    private final ServiceUser serviceUser;
    private final ServiceAquarium serviceAquarium;
    private final ServiceLabel serviceLabel;
    //private final ServiceFile serviceFile;
    //private final Map<String, BiConsumer<Model, DtoUser>> pageContentModelMap = new HashMap<>();
    private final Map<String, DtoItemTypeCard> itemTypeCardMap = new HashMap<>();
    private final Map<String, String> itemTypeTitleMap = new HashMap<>();
    private final Map<String, String> itemTypeDescriptionMap = new HashMap<>();

    private final Map<String, ServiceItem> serviceByName = new HashMap<>();

    @Autowired
    public ServicePage(ServiceUser serviceUser, ServiceAquarium serviceAquarium, ServiceLabel serviceLabel/*, ServiceFile serviceFile*/) {
        this.serviceUser = serviceUser;
        this.serviceAquarium = serviceAquarium;
        this.serviceLabel = serviceLabel;
        //this.serviceFile = serviceFile;
        initServiceByNameMap();
        //setPageContentModelMap();
        setItemTypeTitleAndDescriptionMap();
        setItemTypeCardMap();
    }

    @Override
    @Overload
    public String getPageByName(String pageName, Model model) {
        setPageDataToModel(pageName, null, null, model);
        return "pageTemplate";
    }

    @Override
    @Overload
    public String getPagePartsByName(String templateName, Model model) {
        setPageDataToModel(templateName, null, null, model);
        return "pagePart";
    }

    @Override
    @Overload
    public String getPageByName(String templateName, DtoUser contentOwner, Model model) {
        setPageDataToModel(templateName, contentOwner, null, model);
        return "pageTemplate";
    }

    @Override
    @Overload
    public String getPagePartsByName(String templateName, DtoUser contentOwner, Model model) {
        setPageDataToModel(templateName, contentOwner, null, model);
        return "pagePart";
    }

    @Override
    @Overload
    public String getPageByName(String templateName, UUID itemId, Model model) {
        setPageDataToModel(templateName, null, itemId, model);
        return "pageTemplate";
    }

    @Override
    @Overload
    public String getPagePartsByName(String templateName, UUID itemId, Model model) {
        setPageDataToModel(templateName, null, itemId, model);
        return "pagePart";
    }

    private void setPageDataToModel(String templateName, DtoUser contentOwner, UUID itemId, Model model) {
        DtoUser currentUser = serviceUser.getCurrentUser();
        String itemTypeName = Arrays.stream(ItemType.values())
                .filter(itemType -> templateName.toLowerCase().startsWith(itemType.name().toLowerCase()))
                .findFirst()
                .map(itemType -> itemType.name().toLowerCase())
                .orElse(null);
        model.addAttribute("templateName", templateName);
        model.addAttribute("user", currentUser);
        model.addAttribute("itemTypes", itemTypeCardMap.values());
        if (itemTypeName != null) {
            if (templateName.endsWith("Self") && currentUser.getRole() != UserRole.GUEST) {
                model.addAttribute("allLabels", serviceLabel.getAllForCurrentUserByItemType(ItemType.valueOf(itemTypeName.toUpperCase())));
                model.addAttribute(itemTypeName + "s", serviceByName.get(itemTypeName).getAllForUser(currentUser.getId(), true));
            } else if (templateName.endsWith("User")) {
                model.addAttribute("contentOwner", contentOwner);
                model.addAttribute(itemTypeName + "s", contentOwner == null ? new ArrayList<>() : serviceByName.get(itemTypeName).getAllForUser(contentOwner.getId(), false));
            } else if (templateName.endsWith("Item")) {
                ServiceItem serviceItem = serviceByName.get(itemTypeName);
                model.addAttribute("itemId", itemId);
                DtoItemAbstract dtoItem = serviceByName.get(itemTypeName).getItem(itemId);
                model.addAttribute("dtoItem", dtoItem);
                if (dtoItem!= null) {
                    serviceItem.clearAddedImages(dtoItem);
                    model.addAttribute("contentOwner", dtoItem.getOwner());
                    if (currentUser.getId().equals(dtoItem.getOwner().getId())) {
                        model.addAttribute("allLabels", serviceLabel.getAllForCurrentUserByItemType(ItemType.valueOf(itemTypeName.toUpperCase())));
                    }
                    model.addAttribute("images", serviceItem.getImagesForItem(dtoItem));
                }
            }
        }
    }

    private void setItemTypeCardMap() {
        Arrays.stream(ItemType.values())
                .forEach(itemType ->
                        itemTypeCardMap.put(itemType.name(), DtoItemTypeCard.builder()
                                .typeName(itemType.name().toLowerCase())
                                .name(itemTypeTitleMap.get(itemType.name().toLowerCase()))
                                .description(itemTypeDescriptionMap.get(itemType.name().toLowerCase()))
                                .iconFile("/itemImage/icon_main_" + itemType.name().toLowerCase() + ".png")
                                .build())
                );
    }

    private void initServiceByNameMap() {
        serviceByName.put("aquarium", serviceAquarium);
/*        serviceByName.put("device", "Мое оборудование");
        serviceByName.put("pet", "Мои питомцы");
        serviceByName.put("plant", "Мои растения");
        serviceByName.put("note", "Мои заметки");
        serviceByName.put("task", "Мои задачи");
        serviceByName.put("experiment", "Мои эксперименты");*/
    }

    private void setItemTypeTitleAndDescriptionMap() {
        itemTypeTitleMap.put("aquarium", "Мои аквариумы");
        itemTypeTitleMap.put("device", "Мое оборудование");
        itemTypeTitleMap.put("pet", "Мои питомцы");
        itemTypeTitleMap.put("plant", "Мои растения");
        itemTypeTitleMap.put("note", "Мои заметки");
        itemTypeTitleMap.put("task", "Мои задачи");
        itemTypeTitleMap.put("experiment", "Мои эксперименты");
        itemTypeDescriptionMap.put("aquarium", "Description для карточки \"Мои аквариумы\" на главной");
        itemTypeDescriptionMap.put("device", "Description для карточки \"Мое оборудование\" на главной");
        itemTypeDescriptionMap.put("pet", "Description для карточки \"Мои питомцы\" на главной");
        itemTypeDescriptionMap.put("plant", "Description для карточки \"Мои растения\" на главной");
        itemTypeDescriptionMap.put("note", "Description для карточки \"Мои заметки\" на главной");
        itemTypeDescriptionMap.put("task", "Description для карточки \"Мои задачи\" на главной");
        itemTypeDescriptionMap.put("experiment", "Description для карточки \"Мои эксперименты\" на главной");
    }

}
