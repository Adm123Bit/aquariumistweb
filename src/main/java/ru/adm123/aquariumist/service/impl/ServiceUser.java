package ru.adm123.aquariumist.service.impl;

import lombok.SneakyThrows;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.adm123.aquariumist.domain.dto.DtoActionResult;
import ru.adm123.aquariumist.domain.dto.DtoItemAbstract;
import ru.adm123.aquariumist.domain.dto.DtoSession;
import ru.adm123.aquariumist.domain.dto.DtoUser;
import ru.adm123.aquariumist.domain.dto.form.DtoFormFile;
import ru.adm123.aquariumist.domain.entity.EntityUser;
import ru.adm123.aquariumist.enums.UserRole;
import ru.adm123.aquariumist.repository.RepositoryItem;
import ru.adm123.aquariumist.repository.RepositoryUser;

import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.util.Optional;
import java.util.UUID;

@Service
public class ServiceUser implements ru.adm123.aquariumist.service.ServiceUser {

    private final RepositoryUser repositoryUser;
    private final RepositoryItem repositoryItem;
    private final HttpSession httpSession;
    private final ModelMapper modelMapper;
    private final ServiceFile serviceFile;

    @Autowired
    public ServiceUser(RepositoryUser repositoryUser, RepositoryItem repositoryItem, ModelMapper modelMapper, ServiceFile serviceFile, HttpSession httpSession) {
        this.repositoryUser = repositoryUser;
        this.repositoryItem = repositoryItem;
        this.modelMapper = modelMapper;
        this.serviceFile = serviceFile;
        this.httpSession = httpSession;
    }

    @Override
    @Transactional
    public DtoUser getUserById(long userId) {
        return repositoryUser.findById(userId)
                .map(entityUser -> modelMapper.map(entityUser, DtoUser.class))
                .orElse(null);
    }

    @Override
    @Transactional
    public DtoUser getUserByLogin(String userLogin) {
        if (userLogin == null || userLogin.trim().isEmpty()) {
            return null;
        }
        return repositoryUser.findByLogin(userLogin)
                .map(entityUser -> modelMapper.map(entityUser, DtoUser.class))
                .orElse(null);
    }

    @Override
    @Transactional
    public boolean isUserExists(String login) {
        Optional<EntityUser> entityUserAuthOptional = repositoryUser.findByLogin(login);
        return entityUserAuthOptional.isPresent();
    }

    @Override
    public DtoUser getCurrentUser() {
        DtoSession dtoSession = (DtoSession) httpSession.getAttribute("userSession"/*httpSession.getId()*/);
        if (dtoSession == null) {
            return null;
        }
        return dtoSession.getUser();
    }

    @Override
    @Transactional
    public DtoUser addUser(String login, String password) {
        EntityUser entityUser = new EntityUser();
        entityUser.setLogin(login);
        entityUser.setPassword(encodePassword(password));
        entityUser.setRole(UserRole.USER);
        entityUser = repositoryUser.save(entityUser);
        DtoUser newUser = modelMapper.map(entityUser, DtoUser.class);
        serviceFile.createUserFilePath(newUser);
        return newUser;
    }

    @Override
    @Transactional
    public DtoUser getUserByLoginAndPassword(String login, String password) {
        return repositoryUser.findByLoginAndPassword(login, encodePassword(password))
                .map(entityUser -> modelMapper.map(entityUser, DtoUser.class))
                .orElse(null);
        /*Optional<EntityUser> entityUserOptional = repositoryUser.findByLogin(login);
        if (entityUserOptional.isPresent()) {
            if (entityUserOptional.get().getPassword().equals(encodePassword(password))) {
                return entityUserOptional.map(entityUser -> modelMapper.map(entityUser, DtoUser.class)).orElse(null);
            }
        }
        return null;*/
    }

/*    @Override
    public void setUserAvatar(*//*DtoUser dtoUser*//*) {
        DtoUser currentUser = getCurrentUser();
        currentUser.setAvatar(serviceFile.findByName(serviceFile.getUserFilePath(currentUser.getLogin()), "avatar")
                .map(file -> "avatar/" + currentUser.getLogin() + "/" + file.getName())
                .orElse("defaultImage/avatar.png"));
    }*/

    @Override
    @Transactional
    //@ForAuthorizedUser
    public void setUserPassword(String newPassword) {
        EntityUser entityUser = modelMapper.map(getCurrentUser(), EntityUser.class);//getEntityUserById(getCurrentUser().getId());
        entityUser.setPassword(encodePassword(newPassword));
        repositoryUser.save(entityUser);
    }

    @Override
    public boolean setUserAvatar(DtoFormFile dtoFormFile) {
        DtoUser currentUser = getCurrentUser();
        if (currentUser.getRole() != UserRole.GUEST) {
            Path targetFilePath = serviceFile.getUserFilePath(currentUser.getLogin());
            if (!Files.exists(targetFilePath) && serviceFile.mkDir(targetFilePath) == null) {
                return false;
            }
            File newAvatar = serviceFile.saveBase64File(targetFilePath, dtoFormFile.getFileName(), dtoFormFile.getFileBase64Body());
            if (newAvatar != null) {
                currentUser.setAvatar(serviceFile.findByName(serviceFile.getUserFilePath(currentUser.getLogin()), "avatar")
                        .map(file -> "avatar/" + currentUser.getLogin() + "/" + file.getName())
                        .orElse("defaultImage/avatar.png"));
                return serviceFile.deleteFilesWithExclude(targetFilePath, newAvatar.getName());
            }
            return false;
        }
        return false;
    }

    @Override
    public String getUserAvatar(DtoUser dtoUser) {
        return serviceFile.findByName(serviceFile.getUserFilePath(dtoUser.getLogin()), "avatar")
                .map(file -> "avatar/" + dtoUser.getLogin() + "/" + file.getName())
                .orElse("defaultImage/avatar.png");
    }

    @Override
    public DtoUser getContentOwnerById(Long userId) {
        DtoUser currentUser = getCurrentUser();
        if (currentUser != null && (userId == null || currentUser.getId().equals(userId))) {
            return currentUser;
        } else if (currentUser == null && userId == null) {
            return null;
        }
        return getUserById(userId);
    }

    @Override
    public boolean isUserOwnerOfItem(String userLogin, UUID itemId) {
        return repositoryItem.findById(itemId)
                .map(entityItemAbstract -> entityItemAbstract.getOwner().getLogin().equals(userLogin))
                .orElse(false);
    }

    @SneakyThrows
    public String encodePassword(String password) {
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        messageDigest.update(password.getBytes());
        return DatatypeConverter.printHexBinary(messageDigest.digest());
    }

}
