package ru.adm123.aquariumist.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.adm123.aquariumist.aspect.annotation.IfCurrentUserIsOwner;
import ru.adm123.aquariumist.domain.dto.DtoItemAbstract;
import ru.adm123.aquariumist.domain.dto.DtoItemAquarium;
import ru.adm123.aquariumist.domain.dto.DtoLabel;
import ru.adm123.aquariumist.domain.dto.form.DtoFormFile;
import ru.adm123.aquariumist.domain.dto.form.DtoFormItemAbstract;
import ru.adm123.aquariumist.domain.dto.form.DtoFormItemAquariumAdd;
import ru.adm123.aquariumist.domain.dto.form.DtoFormItemAquariumEdit;
import ru.adm123.aquariumist.domain.entity.EntityItemAbstract;
import ru.adm123.aquariumist.repository.RepositoryItem;

import java.io.File;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

@Service
public interface ServiceItem<T extends DtoItemAbstract, FA extends DtoFormItemAbstract, FE extends DtoFormItemAbstract> {

    Collection<T> getAllForUser(long userId, boolean showPrivate);

    T addItem(FA dtoFormItemAdd);

    void updateItem(FE dtoFormItemEdit);

    T getItem(UUID itemId);

    //boolean deleteImages(Collection<DtoFormFile> dtoFormFiles);

    boolean deleteItem(UUID itemId);

    boolean unArchiveItem(UUID itemId);

    boolean deleteItems(Collection<UUID> itemIds);

    boolean archiveItems(Collection<UUID> itemIds);

    boolean unarchiveItems(Collection<UUID> itemIds);

    boolean addItemImage(DtoFormFile dtoFormFile);

    boolean deleteItemImages(Collection<DtoFormFile> dtoFormFiles);

    void clearAddedImages(/*UUID itemId*/DtoItemAbstract dtoItemAbstract);

    //Collection<String> getImagesForItem(DtoItemAbstract dtoItemAbstract);

    Collection<DtoFormFile> getImagesForItem(DtoItemAbstract dtoItemAbstract);

    //Collection<DtoLabel> getLabelsForItem(EntityItemAbstract entityItemAbstract);

    String getItemCover(DtoItemAbstract dtoItemAbstract);

    EntityItemAbstract getEntityForCurrentUser(UUID entityId);

    //boolean setEditDateAsNow(UUID itemId);

}
