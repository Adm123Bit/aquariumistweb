package ru.adm123.aquariumist.service;

import ru.adm123.aquariumist.domain.dto.DtoItemAquarium;
import ru.adm123.aquariumist.domain.dto.DtoLabel;
import ru.adm123.aquariumist.domain.dto.form.DtoFormItemAquariumAdd;
import ru.adm123.aquariumist.domain.dto.form.DtoFormItemAquariumEdit;
import ru.adm123.aquariumist.domain.dto.form.DtoFormLabelAdd;
import ru.adm123.aquariumist.enums.ItemType;

import java.util.Collection;
import java.util.UUID;

public interface ServiceLabel {

    Collection<DtoLabel> getAllForCurrentUserByItemType(ItemType itemType);

    //Collection<DtoLabel> getAllById(Collection<UUID> labelIds);

    DtoLabel addLabel(DtoFormLabelAdd dtoFormLabelAdd);

}
