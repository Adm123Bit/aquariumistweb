package ru.adm123.aquariumist.aspect;

import lombok.SneakyThrows;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.adm123.aquariumist.domain.dto.DtoItemAquarium;
import ru.adm123.aquariumist.domain.dto.DtoUser;
import ru.adm123.aquariumist.enums.ItemShare;
import ru.adm123.aquariumist.enums.UserRole;
import ru.adm123.aquariumist.service.impl.ServiceUser;

import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

@Aspect
@Component
public class FilterByShare {

    private final ServiceUser serviceUser;

    @Autowired
    public FilterByShare(ServiceUser serviceUser) {
        this.serviceUser = serviceUser;
    }

    @Pointcut("@annotation(ru.adm123.aquariumist.aspect.annotation.FilterByShare)")
    public void shareFilterAnnotation() {
    }

    @Pointcut("execution(java.util.Collection<ru.adm123.aquariumist.domain.dto.DtoItemAquarium> ru.adm123.aquariumist.service.impl.ServiceAquarium.*(long, boolean)) && args(userId, showPrivate)")
    public void filterExecutionAquariumList(long userId, boolean showPrivate) {
    }

    @Pointcut("execution(ru.adm123.aquariumist.domain.dto.DtoItemAquarium ru.adm123.aquariumist.service.impl.ServiceAquarium.*(java.util.UUID)) && args(itemId)")
    public void filterExecutionAquariumItem(UUID itemId) {
    }

    @SneakyThrows
    @SuppressWarnings("unchecked")
    @Around("shareFilterAnnotation() && filterExecutionAquariumList(userId, showPrivate)")
    public Object filterByRole(ProceedingJoinPoint pjp, long userId, boolean showPrivate) {
        DtoUser currentUser = serviceUser.getCurrentUser();
        UserRole userRole = currentUser.getRole();
        Collection<DtoItemAquarium> retCollection = (Collection<DtoItemAquarium>) pjp.proceed();
        return retCollection.stream()
                .filter(aquarium -> {
                    if (!showPrivate && aquarium.getShare() == ItemShare.PRIVATE) {
                        return false;
                    }
                    if (currentUser.getId() == userId) {
                        return true;
                    }
                    if (userRole == UserRole.GUEST) {
                        return aquarium.getShare() == ItemShare.COMMON;
                    } else {
                        return aquarium.getShare() == ItemShare.COMMON || aquarium.getShare() == ItemShare.APPUSER;
                    }
                })
                .collect(Collectors.toList());
    }

    @SneakyThrows
    @Around("shareFilterAnnotation() && filterExecutionAquariumItem(itemId)")
    public Object filterByRole(ProceedingJoinPoint pjp, UUID itemId) {
        DtoUser currentUser = serviceUser.getCurrentUser();
        UserRole userRole = currentUser.getRole();
        DtoItemAquarium retItem = (DtoItemAquarium) pjp.proceed();
        if (retItem != null) {
            if (retItem.getOwner().getId().equals(currentUser.getId()) || retItem.getShare() == ItemShare.COMMON) {
                return retItem;
            } else if (currentUser.getRole() != UserRole.GUEST && retItem.getShare() != ItemShare.PRIVATE) {
                return retItem;
            }
        }
        return null;
    }

}
