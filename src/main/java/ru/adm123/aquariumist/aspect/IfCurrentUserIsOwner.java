package ru.adm123.aquariumist.aspect;

import lombok.SneakyThrows;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.adm123.aquariumist.domain.dto.DtoItemAbstract;
import ru.adm123.aquariumist.domain.dto.DtoUser;
import ru.adm123.aquariumist.enums.ItemShare;
import ru.adm123.aquariumist.enums.UserRole;
import ru.adm123.aquariumist.service.ServiceItem;
import ru.adm123.aquariumist.service.impl.ServiceUser;

import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

@Aspect
@Component
public class IfCurrentUserIsOwner {

    private final ServiceUser serviceUser;
    private final ServiceItem serviceItem;

    @Autowired
    public IfCurrentUserIsOwner(ServiceUser serviceUser, ServiceItem serviceItem) {
        this.serviceUser = serviceUser;
        this.serviceItem = serviceItem;
    }

    @Pointcut("@annotation(ru.adm123.aquariumist.aspect.annotation.IfCurrentUserIsOwner)")
    public void ownerFilterAnnotation() {
    }

    @Pointcut("execution(boolean ru.adm123.aquariumist.service.impl.*.*(java.util.UUID)) && args(itemId)")
    public void filterExecutionItem(UUID itemId) {
    }

    @Pointcut("execution(boolean ru.adm123.aquariumist.service.impl.*.*(java.util.Collection<java.util.UUID>)) && args(java.util.Collection<itemIds>)")
    public void filterExecutionItemCollection(Collection<UUID> itemIds) {
    }

    @SneakyThrows
    @Around("ownerFilterAnnotation() && filterExecutionItem(itemId)")
    public Object checkCurrentUserIsOwnerOfItem(ProceedingJoinPoint pjp, UUID itemId) {
        DtoUser currentUser = serviceUser.getCurrentUser();
        if (currentUser.getRole() == UserRole.GUEST) {
            return false;
        }
        DtoItemAbstract dtoItemAbstract = serviceItem.getItem(itemId);
        if (dtoItemAbstract == null) {
            return false;
        }
        if (!dtoItemAbstract.getOwner().getId().equals(currentUser.getId())) {
            return false;
        }
        return pjp.proceed();
    }

    @SneakyThrows
    @SuppressWarnings("unchecked")
    @Around("ownerFilterAnnotation() && filterExecutionItemCollection(itemIds)")
    public Object checkCurrentUserIsOwnerOfItemCollection(ProceedingJoinPoint pjp, Collection<UUID> itemIds) {
        DtoUser currentUser = serviceUser.getCurrentUser();
        if (currentUser.getRole() == UserRole.GUEST) {
            return false;
        }
        Collection<DtoItemAbstract> dtoItems = serviceItem.getAllForUser(currentUser.getId(), true);
        if (dtoItems.size() == 0) {
            return false;
        }
        Collection<DtoItemAbstract> equalOwnerItemList = dtoItems.stream()
                .filter(dtoItemAbstract -> itemIds.contains(dtoItemAbstract.getId()))
                .collect(Collectors.toList());
        if (equalOwnerItemList.size() != itemIds.size()) {
            return false;
        }
        return pjp.proceed();
    }

}
