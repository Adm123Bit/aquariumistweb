package ru.adm123.aquariumist.validator;

public interface ObjectValidator {

    boolean validate(Object object);

}
