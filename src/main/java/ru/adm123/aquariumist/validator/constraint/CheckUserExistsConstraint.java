package ru.adm123.aquariumist.validator.constraint;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import ru.adm123.aquariumist.validator.ObjectValidator;
import ru.adm123.aquariumist.validator.annotation.CheckUserExists;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class CheckUserExistsConstraint implements ConstraintValidator<CheckUserExists, Object>, ApplicationContextAware {

    private ApplicationContext applicationContext;
    private ObjectValidator objectValidator;

    @Override
    public void initialize(ru.adm123.aquariumist.validator.annotation.CheckUserExists constraintAnnotation) {
        Class<? extends ObjectValidator> validator = constraintAnnotation.objectValidator();
        if (validator.isInterface()) {
            throw new IllegalArgumentException("Validator must be a class, not interface");
        }
        objectValidator = applicationContext.getBean(validator);
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        return objectValidator.validate(value);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
