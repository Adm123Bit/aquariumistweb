package ru.adm123.aquariumist.validator.constraint;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import ru.adm123.aquariumist.validator.ObjectValidator;
import ru.adm123.aquariumist.validator.annotation.CheckCurrentUser;
import ru.adm123.aquariumist.validator.annotation.CheckUserExists;
import ru.adm123.aquariumist.validator.impl.CurrentUserNotNull;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class CheckCurrentUserConstraint implements ConstraintValidator<CheckCurrentUser, Object>, ApplicationContextAware {

    private CurrentUserNotNull validator;

    @Override
    public void initialize(CheckCurrentUser constraintAnnotation) {}

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        return validator.validate();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        validator = applicationContext.getBean(CurrentUserNotNull.class);
    }

}
