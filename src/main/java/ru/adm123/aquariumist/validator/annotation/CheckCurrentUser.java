package ru.adm123.aquariumist.validator.annotation;

import ru.adm123.aquariumist.validator.constraint.CheckCurrentUserConstraint;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Constraint(validatedBy = CheckCurrentUserConstraint.class)
@Target(TYPE)
@Retention(RUNTIME)
public @interface CheckCurrentUser {

    String message();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
