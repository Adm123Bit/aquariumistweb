package ru.adm123.aquariumist.validator.annotation;

import ru.adm123.aquariumist.validator.ObjectValidator;
import ru.adm123.aquariumist.validator.constraint.CheckUserExistsConstraint;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Constraint(validatedBy = CheckUserExistsConstraint.class)
@Target(TYPE)
@Retention(RUNTIME)
public @interface CheckUserExists {

    Class<? extends ObjectValidator> objectValidator();

    String message();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
