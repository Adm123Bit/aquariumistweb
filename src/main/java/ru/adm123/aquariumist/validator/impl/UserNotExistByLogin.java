package ru.adm123.aquariumist.validator.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.adm123.aquariumist.domain.dto.form.DtoFormUserRegister;
import ru.adm123.aquariumist.repository.RepositoryUser;
import ru.adm123.aquariumist.validator.ObjectValidator;

@Component
public class UserNotExistByLogin implements ObjectValidator {

    @Autowired
    private RepositoryUser repositoryUser;

    @Override
    public boolean validate(Object object) {
        return !repositoryUser.findByLogin(((DtoFormUserRegister) object).getLogin()).isPresent();
    }

}
