package ru.adm123.aquariumist.validator.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.adm123.aquariumist.domain.dto.form.DtoFormUserLogin;
import ru.adm123.aquariumist.domain.dto.form.DtoFormUserRegister;
import ru.adm123.aquariumist.repository.RepositoryUser;
import ru.adm123.aquariumist.service.impl.ServiceUser;
import ru.adm123.aquariumist.validator.ObjectValidator;

@Component
public class UserExistByLoginAndPassword implements ObjectValidator {

    @Autowired
    private ServiceUser serviceUser;

    @Override
    public boolean validate(Object object) {
        return serviceUser.getUserByLoginAndPassword(((DtoFormUserLogin) object).getLogin(), ((DtoFormUserLogin) object).getPassword()) != null;
    }

}
