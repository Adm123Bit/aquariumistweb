package ru.adm123.aquariumist.validator.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.adm123.aquariumist.domain.dto.DtoItemAquarium;
import ru.adm123.aquariumist.domain.dto.form.DtoFormItemAquariumEdit;
import ru.adm123.aquariumist.domain.dto.form.DtoFormUserRegister;
import ru.adm123.aquariumist.repository.RepositoryAquarium;
import ru.adm123.aquariumist.repository.RepositoryUser;
import ru.adm123.aquariumist.validator.ObjectValidator;

@Component
public class AquariumExistById implements ObjectValidator {

    @Autowired
    private RepositoryAquarium repositoryAquarium;

    @Override
    public boolean validate(Object object) {
        return repositoryAquarium.findById(((DtoFormItemAquariumEdit) object).getId()).isPresent();
    }

}
