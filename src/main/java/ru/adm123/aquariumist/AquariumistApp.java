package ru.adm123.aquariumist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class AquariumistApp {

    public static void main(String[] args) {
        SpringApplication.run(AquariumistApp.class, args);
    }

}
